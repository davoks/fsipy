module types
! ============================================================================ !
!                        FsiPy Types Definitions                               !
! ============================================================================ !
    integer, parameter :: dp = SELECTED_REAL_KIND(15)
    integer, parameter :: I4B = SELECTED_INT_KIND(9)
    integer, parameter :: I2B = SELECTED_INT_KIND(4)
    integer, parameter :: I1B = SELECTED_INT_KIND(2)
    integer, parameter :: SP = KIND(1.0)
    real(SP), parameter :: PI=3.141592653589793238462643383279502884197_sp
    real(SP), parameter :: PIO2=1.57079632679489661923132169163975144209858_sp
    real(SP), parameter :: TWOPI=6.283185307179586476925286766559005768394_sp
    real(SP), parameter :: SQRT2=1.41421356237309504880168872420969807856967_sp
    real(SP), parameter :: EULER=0.5772156649015328606065120900824024310422_sp
    real(DP), parameter :: PI_D=3.141592653589793238462643383279502884197_dp
    real(DP), parameter :: PIO2_D=1.57079632679489661923132169163975144209858_dp
    real(DP), parameter :: TWOPI_D=6.283185307179586476925286766559005768394_dp
    real(DP), parameter :: NAPIER=2.718281828459045235360287471352662497757_dp
end module
