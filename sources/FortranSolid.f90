module FortranSolid
! ============================================================================ !
!                        FsiPy Fortran Solid Module                            !
! ============================================================================ !
    use types
    use FortranKernel

    implicit none

    type, extends(Equation), public :: ExplicitSolid
    contains
        procedure, pass(this), public :: compute_gauss_lhs_rhs => compute_gauss_lhs_rhs_explicit_solid
        procedure, pass(this), public :: compute_gauss_lhs     => compute_gauss_lhs_explicit_solid
        procedure, pass(this), public :: compute_gauss_rhs     => compute_gauss_rhs_explicit_solid
        procedure, pass(this), public :: compute_gauss_force   => compute_gauss_force_explicit_solid
        procedure, pass(this), public :: compute_natural_bcs   => compute_natural_bcs_explicit_solid
    end type
    !
    ! Global variables
    !
    public
    real(8), allocatable :: gp_rho(:,:)
    real(8), allocatable :: gp_young(:,:)
    real(8)              :: alpha
    real(8)              :: poisson

contains

subroutine dealloc()
    deallocate( l_elems )
    deallocate( gauss_weights )
    deallocate( gp_jacobians )
    deallocate( gp_inv_jacobians )
    deallocate( gp_det_jacobians )
    deallocate( gp_coords )
    deallocate( gp_shape )
    deallocate( gp_dshape )
    deallocate( gp_volume )
    deallocate( gp_dshape_glo )
    deallocate( gp_shape_shape )
    deallocate( gp_shape_dshape )
    deallocate( gp_dshape_shape )
    deallocate( gp_dshape_dshape )
    deallocate( gp_rho )
    deallocate( gp_young )
end subroutine


subroutine initialize(l_elems_in,          &
                      gauss_weights_in,    &
                      gp_jacobians_in,     &
                      gp_inv_jacobians_in, &
                      gp_det_jacobians_in, &
                      gp_coords_in,        &
                      gp_shape_in,         &
                      gp_dshape_in,        &
                      gp_volume_in,        &
                      gp_rho_in,           &
                      gp_young_in,         &
                      material_in,         &
                      dt_in,               &
                      alpha_in,            &
                      poisson_in,          &
                      nnode_in,            &
                      ndofs_total_in,      &
                      npast_in,            &
                      ndime_in,            &
                      nelem_in,            &
                      nnode_elem_in,       &
                      ngaus_in,            &
                      nbasis_in)
    integer(4),       intent(in) :: l_elems_in(nelem_in,nnode_elem_in)
    real(8),          intent(in) :: gauss_weights_in(ngaus_in)
    real(8),          intent(in) :: gp_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_inv_jacobians_in(nelem_in,ngaus_in,ndime_in,ndime_in)
    real(8),          intent(in) :: gp_det_jacobians_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_coords_in(nelem_in,ngaus_in,ndime_in)
    real(8),          intent(in) :: gp_shape_in(ngaus_in,nbasis_in)
    real(8),          intent(in) :: gp_dshape_in(ngaus_in,nbasis_in,ndime_in)
    real(8),          intent(in) :: gp_volume_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_rho_in(nelem_in,ngaus_in)
    real(8),          intent(in) :: gp_young_in(nelem_in,ngaus_in)
    character(len=7), intent(in) :: material_in
    real(8),          intent(in) :: dt_in
    real(8),          intent(in) :: alpha_in
    real(8),          intent(in) :: poisson_in
    integer(4) :: nnode_in
    integer(4) :: ndofs_total_in
    integer(4) :: npast_in
    integer(4) :: ndime_in
    integer(4) :: nelem_in
    integer(4) :: nnode_elem_in
    integer(4) :: ngaus_in
    integer(4) :: nbasis_in

    call initialize_kernel(l_elems_in,          &
                           gauss_weights_in,    &
                           gp_jacobians_in,     &
                           gp_inv_jacobians_in, &
                           gp_det_jacobians_in, &
                           gp_coords_in,        &
                           gp_shape_in,         &
                           gp_dshape_in,        &
                           gp_volume_in,        &
                           material_in,         &
                           dt_in,               &
                           nnode_in,            &
                           ndofs_total_in,      &
                           npast_in,            &
                           ndime_in,            &
                           nelem_in,            &
                           nnode_elem_in,       &
                           ngaus_in,            &
                           nbasis_in)

    ! Allocate global arrays required in FortranSolid
    allocate( gp_shape_dshape(nelem,ngaus,nnode_elem,nnode_elem,ndime) )
    allocate( gp_dshape_shape(nelem,ngaus,nnode_elem,nnode_elem,ndime) )
    allocate( gp_dshape_dshape(nelem,ngaus,nnode_elem,nnode_elem)      )
    allocate( gp_young(nelem,ngaus)                                    )
    allocate( gp_rho(nelem,ngaus)                                      )

    ! Save FortranSolid's global parameters -> **DEFINE PASSING**
    gp_young = gp_young_in
    gp_rho   = gp_rho_in
    alpha    = alpha_in
    poisson  = poisson_in

    ! Calculate elemental matrices required in FortranSolid
    call compute_laplacian_matrix()
    call compute_gradient_matrix()
    call compute_divergence_matrix()
end subroutine


subroutine assemble_algebraic_system_from_python(lhs,               &
                                                 idlhs,             &
                                                 rhs,               &
                                                 equation_name,     &
                                                 variables,         &
                                                 source,            &
                                                 levelset,          &
                                                 flag_assemble_lhs, &
                                                 tcount,            &
                                                 ndofs_in,          &
                                                 nrows_in,          &
                                                 nnode_in,          &
                                                 ndofs_total_in,    &
                                                 npast_in,          &
                                                 lhs_lump)
    real(8),      intent(out) :: lhs(nrows_in)
    integer(4),   intent(out) :: idlhs(nrows_in, 4)
    real(8),      intent(out) :: rhs(ndofs_in*nnode_in)
    character(*), intent(in)  :: equation_name
    real(8),      intent(in)  :: variables(ndofs_total_in, nnode_in, npast_in)
    real(8),      intent(in)  :: source(ndofs_total_in, nnode_in)
    real(8),      intent(in)  :: levelset(nnode_in)
    logical,      intent(in)  :: flag_assemble_lhs
    integer(4),   intent(in)  :: tcount 
    integer(4),   intent(in)  :: nrows_in
    integer(4),   intent(in)  :: nnode_in
    integer(4),   intent(in)  :: ndofs_total_in
    integer(4),   intent(in)  :: ndofs_in
    integer(4),   intent(in)  :: npast_in
    real(8),      intent(out) :: lhs_lump(ndofs_in*nnode_in)    
    type(ExplicitSolid)       :: oexplicit_solid

    call oexplicit_solid % assemble_algebraic_system(lhs,               &
                                                     idlhs,             &
                                                     rhs,               &
                                                     variables,         &
                                                     source,            &
                                                     levelset,          &
                                                     flag_assemble_lhs, &
                                                     tcount,            &
                                                     ndofs_in,          &
                                                     lhs_lump)
end subroutine


subroutine assemble_force_from_python(force,          &
                                      equation_name,  &
                                      variables,      &
                                      source,         &
                                      ndofs_in,       &
                                      nrows_in,       &
                                      nnode_in,       &
                                      ndofs_total_in, &
                                      npast_in)
    real(8),      intent(out) :: force(ndofs_in*nnode_in)
    character(*), intent(in)  :: equation_name
    real(8),      intent(in)  :: variables(ndofs_total_in, nnode_in, npast_in)
    real(8),      intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),   intent(in)  :: nrows_in
    integer(4),   intent(in)  :: nnode_in
    integer(4),   intent(in)  :: ndofs_total_in
    integer(4),   intent(in)  :: ndofs_in
    integer(4),   intent(in)  :: npast_in
    type(ExplicitSolid)       :: oexplicit_solid

    call oexplicit_solid % assemble_force(force,        &
                                          variables,    &
                                          source,       &
                                          ndofs_in)
end subroutine


subroutine get_mass_matrix(mass_matrix_out, &
                           idmass_out,      &
                           nelem_in,        &
                           nnode_elem_in)
    real(8),       intent(out) :: mass_matrix_out(nelem_in*nnode_elem_in*nnode_elem_in)
    integer(4),    intent(out) :: idmass_out(nelem_in*nnode_elem_in*nnode_elem_in, 2)
    integer(4),    intent(in)  :: nelem_in
    integer(4),    intent(in)  :: nnode_elem_in
    call compute_global_mass_matrix(mass_matrix_out, idmass_out)
end subroutine


subroutine compute_gauss_lhs_rhs_explicit_solid(this,           & 
                                                gplhs,          &
                                                gprhs,          &
                                                elvar,          &
                                                elsource,       &
                                                elevelset,      &
                                                nnode_in,       &
                                                ndofs_total_in, &
                                                ndofs_in,       &
                                                npast_in,       &
                                                ielem,          &
                                                igaus)
  class(ExplicitSolid), intent(in)  :: this
  real(8),              intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),              intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),              intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),              intent(in)  :: elevelset(nnode_in)
  integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),           intent(in)  :: ielem, igaus
  integer(4)                        :: inode, jnode, idime

  call this % compute_gauss_lhs(gplhs,          &
                                elvar,          &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
  call this % compute_gauss_rhs(gprhs,          &
                                elvar,          &
                                elsource,       &
                                elevelset,      &
                                nnode_in,       &
                                ndofs_total_in, &
                                ndofs_in,       &
                                npast_in,       &
                                ielem,          &
                                igaus)
end subroutine


subroutine compute_gauss_lhs_explicit_solid(this,           & 
                                            gplhs,          &
                                            elvar,          &
                                            elevelset,      &
                                            nnode_in,       &
                                            ndofs_total_in, &
                                            ndofs_in,       &
                                            npast_in,       &
                                            ielem,          &
                                            igaus)
  class(ExplicitSolid), intent(in)  :: this
  real(8),              intent(out) :: gplhs(ndofs_in, nnode_in, ndofs_in, nnode_in)
  real(8),              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),              intent(in)  :: elevelset(nnode_in)
  integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),           intent(in)  :: ielem, igaus
  integer(4)                        :: inode, jnode, idime

  ! Compute LHS contribution
  gplhs = 0.0_dp
  do inode=1,nnode_elem
    do idime=1,ndime
      do jnode=1,nnode_elem
        !
        ! M 
        !
        gplhs(idime,inode,idime,jnode) = gp_rho(ielem,igaus) * gp_shape_shape(igaus,inode,jnode)
      end do
    end do
  end do
end subroutine


subroutine compute_gauss_rhs_explicit_solid(this,           &
                                            gprhs,          &
                                            elvar,          &
                                            elsource,       &
                                            elevelset,      &
                                            nnode_in,       &
                                            ndofs_total_in, &
                                            ndofs_in,       &
                                            npast_in,       &
                                            ielem,          &
                                            igaus)
  class(ExplicitSolid), intent(in)  :: this
  real(8),              intent(out) :: gprhs(ndofs_in, nnode_in)
  real(8),              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),              intent(in)  :: elsource(ndofs_total_in, nnode_in)
  real(8),              intent(in)  :: elevelset(nnode_in)
  integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),           intent(in)  :: ielem, igaus
  real(8)                           :: elacceleration(ndime,nnode_in,npast_in)
  real(8)                           :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                           :: eldisplacement(ndime,nnode_in,npast_in)
  real(8)                           :: elmomentum_source(ndime,nnode_in)
  real(8)                           :: I(ndime,ndime)
  real(8)                           :: B0(nnode_in,ndime)
  real(8)                           :: H(ndime,ndime)
  real(8)                           :: HT(ndime,ndime)
  real(8)                           :: F(ndime,ndime)
  real(8)                           :: FT(ndime,ndime)
  real(8)                           :: E(ndime,ndime)
  real(8)                           :: PK1(ndime,ndime)
  real(8)                           :: PK2(ndime,ndime)
  integer(4)                        :: inode, jnode, knode, idime, jdime

  ! Extract element nodal values of acceleration, velocity and displacement
  elvelocity     = elvar(ndime+1:2*ndime,:,:)
  eldisplacement = elvar(2*ndime+1:,     :,:)
  ! Extract element nodal values of momentum source
  elmomentum_source = elsource(1:ndime,:)
  I = 0.0_dp
  forall(idime = 1:ndime) I(idime,idime) = 1.0_dp

  !
  ! Evaluate material model
  !
  ! Get derivatives of shape functions  
  B0 = gp_dshape_glo(ielem,igaus,:,:)
  ! Calculate displacement gradient
  H = matmul( eldisplacement(:,:,iter_k) , B0(:,:) )
  ! Transpose of displacement gradient
  HT = transpose(H)
  ! Calculate deformation gradient
  F = I + H
  ! Calculate transpose of deformation gradient
  FT = I + HT
  ! Calculate Green-Lagrange strain
  E = 0.5_dp * (H + HT + matmul(HT,H))
  ! Evaluate material model to calculate PK2
  if     (material == 'isoline') then
      call eval_pk2_isolin(E, gp_young(ielem,igaus), ndime, PK2)
  elseif (material == 'neohook') then
      call eval_pk2_neohooke(F, FT, gp_young(ielem,igaus), ndime, PK2)
  end if
  ! Calculate PK1 from PK1
  PK1 = matmul(PK2, FT)

  !
  ! Compute RHS contribution
  !
  gprhs = 0.0_dp
  do inode=1,nnode_elem
      do idime=1,ndime
          do jdime=1,ndime
              !
              ! Internal forces
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 - B0(inode,jdime) &
                                 * PK1(jdime,idime)
          end do

          do jnode=1,nnode_elem
              !
              ! External body forces
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 + gp_rho(ielem,igaus) &
                                 * gp_shape_shape(igaus,inode,jnode)  &
                                 * elmomentum_source(idime,jnode)
              !
              ! Rayleigh damping
              !
              gprhs(idime,inode) = gprhs(idime,inode) &
                                 - alpha &
                                 * gp_shape_shape(igaus,inode,jnode) &
                                 * elvelocity(idime,jnode,iter_k) 
                                 ! + beta*K
          enddo
      enddo
  enddo
end subroutine


subroutine compute_gauss_force_explicit_solid(this,           &
                                              gpforce,        &
                                              elvar,          &
                                              elsource,       &
                                              nnode_in,       &
                                              ndofs_total_in, &
                                              ndofs_in,       &
                                              npast_in,       &
                                              ielem,          &
                                              igaus)
  class(ExplicitSolid), intent(in)  :: this
  real(8),              intent(out) :: gpforce(ndofs_in, nnode_in)
  real(8),              intent(in)  :: elvar(ndofs_total_in, nnode_in, npast_in)
  real(8),              intent(in)  :: elsource(ndofs_total_in, nnode_in)
  integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in, npast_in
  integer(4),           intent(in)  :: ielem, igaus
  real(8)                           :: elacceleration(ndime,nnode_in,npast_in)
  real(8)                           :: elvelocity(ndime,nnode_in,npast_in)
  real(8)                           :: eldisplacement(ndime,nnode_in,npast_in)
  real(8)                           :: elmomentum_source(ndime,nnode_in)
  real(8)                           :: I(ndime,ndime)
  real(8)                           :: B0(nnode_in,ndime)
  real(8)                           :: H(ndime,ndime)
  real(8)                           :: HT(ndime,ndime)
  real(8)                           :: F(ndime,ndime)
  real(8)                           :: FT(ndime,ndime)
  real(8)                           :: E(ndime,ndime)
  real(8)                           :: PK1(ndime,ndime)
  real(8)                           :: PK2(ndime,ndime)
  integer(4)                        :: inode, jnode, knode, idime, jdime

  ! Extract element nodal values of acceleration, velocity and displacement
  eldisplacement = elvar(2*ndime+1:,:,:)
  ! Extract element nodal values of momentum source
  elmomentum_source = elsource(1:ndime,:)
  I = 0.0_dp
  forall(idime = 1:ndime) I(idime,idime) = 1.0_dp

  !
  ! Evaluate material model
  !
  ! Get derivatives of shape functions  
  B0 = gp_dshape_glo(ielem,igaus,:,:)
  ! Calculate displacement gradient
  H = matmul( eldisplacement(:,:,iter_k) , B0(:,:) )
  ! Transpose of displacement gradient
  HT = transpose(H)
  ! Calculate deformation gradient
  F = I + H
  ! Calculate transpose of deformation gradient
  FT = I + HT
  ! Calculate Green-Lagrange strain
  E = 0.5_dp * (H + HT + matmul(HT,H))
  ! Evaluate material model to calculate PK2
  if     (material == 'isoline') then
      call eval_pk2_isolin(E, gp_young(ielem,igaus), ndime, PK2)
  elseif (material == 'neohook') then
      call eval_pk2_neohooke(F, FT, gp_young(ielem,igaus), ndime, PK2)
  end if
  ! Calculate PK1 from PK1
  PK1 = matmul(PK2, FT)

  !
  ! Compute RHS contribution
  !
  gpforce = 0.0_dp
  do inode=1,nnode_elem
      do idime=1,ndime
          do jdime=1,ndime
              !
              ! Internal forces
              !
              gpforce(idime,inode) = gpforce(idime,inode) &
                                   - B0(inode,jdime) &
                                   * PK1(jdime,idime)
          end do

          do jnode=1,nnode_elem
              !
              ! External body forces
              !
              gpforce(idime,inode) = gpforce(idime,inode) &
                                   + gp_rho(ielem,igaus) &
                                   * gp_shape_shape(igaus,inode,jnode)  &
                                   * elmomentum_source(idime,jnode)
          enddo
      enddo
  enddo
end subroutine


subroutine compute_natural_bcs_explicit_solid(this,           &
                                              bcnat,          &
                                              source,         &
                                              nnode_in,       &
                                              ndofs_total_in, &
                                              ndofs_in)
    class(ExplicitSolid), intent(in)  :: this
    real(8),              intent(out) :: bcnat(ndofs_in, nnode_in)
    real(8),              intent(in)  :: source(ndofs_total_in, nnode_in)
    integer(4),           intent(in)  :: nnode_in, ndofs_total_in, ndofs_in
    bcnat(:,:) = 0.0_dp
end subroutine


subroutine eval_pk2_isolin(E, young, ndime_in, PK2)
  real(8),    intent(in)  :: E(ndime_in, ndime_in)
  real(8),    intent(in)  :: young
  integer(4), intent(in)  :: ndime_in
  real(8),    intent(out) :: PK2(ndime_in, ndime_in)
  real(8)                 :: I(ndime_in, ndime_in)
  real(8)                 :: lame1, lame2, trace_E
  integer(4)              :: idime
 
  ! Calculate identity matrix and Lame coefficients
  I = 0.0_dp
  forall(idime = 1:ndime_in) I(idime,idime) = 1.0_dp
  lame1 = poisson * young / ((1.0_dp+poisson)*(1.0_dp-2.0_dp*poisson))
  lame2 = 0.5_dp  * young /  (1.0_dp+poisson)

!  ! If 2D & in plane stress conditions => modify 1st Lame coefficient
!  if (ndime_in==2) then
!      lame1 = 2.0 * lame1 * lame2 / (lame1 + 2.0*lame2)
!  end if

  ! Calculate trace of Green-Lagrange strain
  trace_E = sum( (/ (E(idime,idime), idime=1, ndime_in) /) )

  ! Calculate PK2
  PK2 = lame1*trace_E*I + 2.0_dp*lame2*E
end subroutine 


subroutine eval_pk2_neohooke(F, FT, young, ndime_in, PK2)
  real(8),    intent(in)  :: F(ndime_in, ndime_in)
  real(8),    intent(in)  :: FT(ndime_in, ndime_in)
  real(8),    intent(in)  :: young
  integer(4), intent(in)  :: ndime_in
  real(8),    intent(out) :: PK2(ndime_in, ndime_in)
  real(8)                 :: Cinv(ndime_in, ndime_in)
  real(8)                 :: I(ndime_in, ndime_in)
  real(8)                 :: lame1, lame2, J
  integer(4)              :: idime

  ! Calculate the inversed of the right C-G tensor
  Cinv = matinv(matmul(FT,F))

  ! Calculate determinant of deformation gradient
  J = matdet(F)

  ! Calculate identity matrix
  I = 0.0_dp
  forall(idime = 1:ndime_in) I(idime,idime) = 1.0_dp

  ! Calculate the Lame coefficients
  lame1 = poisson * young / ((1.0_dp+poisson)*(1.0_dp-2.0_dp*poisson))
  lame2 = 0.5_dp  * young /  (1.0_dp+poisson)

!  ! If 2D & in plane stress conditions => modify 1st Lame coefficient
!  if (ndime_in==2) then
!      lame1 = 2.0 * lame1 * lame2 / (lame1 + 2.0*lame2)
!  end if

  ! Calculate PK2
  PK2 = lame1/2.*(J**2-1.)*Cinv + lame2*(I-Cinv)
end subroutine 


subroutine modify_timestep(dt_in)
    real(8), intent(in) :: dt_in
    call change_timestep(dt_in)
end subroutine


end module
