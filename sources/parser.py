"""
    FsiPy parser: reads input files
    =============
"""

import numpy as np
from   scipy.interpolate import interp1d
import glob
import os
import gmsh
import partition as partition
from   ctypes import *
import ast
import copy
from   mpi4py import MPI


def determine_if_parallel(d_params):
    """ Determines if simulation is run in serial or parallel.

        Inputs:
            d_exchange_data : dict with domain partition data.
        Outputs:
            is_parallel : True/False boolian.
        """
    is_parallel = False
    nsubdomains = MPI.COMM_WORLD.Get_size()
    if d_params['physics'] == 'fluid' and nsubdomains > 1:
       is_parallel = True

    return is_parallel


def read_inputs(filename, irank=0):
    """ Reads config and mesh files.

        Inputs
	------
            filename: str
		path to problem config file.
            irank: int
		my MPI rank id.
        Outputs
	-------
            d_params: dict
		contains problem parameters.
            d_mesh: dict
		contains mesh data.
            d_bconds: dict
		contains boundary conditions data.
            d_parallel: dict
		constains nodal exchange arrays.
    """

    # Reads config and mesh files
    d_params = read_config(filename, irank)

    # Determine if the execution has to be perfomed in a parallel environment
    d_params['is_parallel'] = determine_if_parallel(d_params)

    if d_params['physics'] == 'coupled':
        d_mesh     = {}
        d_parallel = {}
        d_bconds   = []
    else:
        d_mesh, d_parallel = read_mesh(filename, d_params, irank)
        d_bconds           = read_boundary_conditions(filename, d_mesh, irank)


        phys_groups = list(zip(*gmsh.model.getPhysicalGroups()))
        phys_tags   = list(phys_groups[1])
        
        # For the parallel version only
        if d_params['is_parallel'] == True:
            # Rebuild the boundary conditions for the current parititon
            phys_groups = list(zip(*gmsh.model.getPhysicalGroups()))
            phys_tags	= list(phys_groups[1])

            d_bconds2 = {}                
            for key in d_bconds.keys():
                d_bconds2[key] = {'nodes': {}}


            for equation_name in d_bconds:
                for btype in d_bconds[equation_name]:
                    for iphysical, boundary_condition in d_bconds[equation_name]['nodes'].items():
                        if iphysical in phys_tags:
                            boundary_condition2 = {'iphysical'	     : boundary_condition['iphysical'],
                                                   'types'	     : boundary_condition['types'],
                                                   'factors'	     : boundary_condition['factors'],
                                                   'function_number' : boundary_condition['function_number'],
                                                   'function'	     : boundary_condition['function'],
                                                   'function_type'   : boundary_condition['function_type']}

                            d_bconds2[equation_name]['nodes'][iphysical] = boundary_condition2
            d_bconds = d_bconds2
                        
        #
        #    print(phys_tags)
        #
        #    
        #    # Loop over boundary conditions
        #    d_bconds2 = {'nodes':{},'elements':{},'zero':{}}
        #    for btype in d_bconds:
        #        for iphysical, bcond in d_bconds[btype].items():
        #            if iphysical in phys_tags:
        #                bcond2 = {'iphysical'       : bcond['iphysical'],
        #                          'types'           : bcond['types'],
        #                          'factors'         : bcond['factors'],
        #                          'function_number' : bcond['function_number'],
        #                          'function'        : bcond['function'],
        #                          'function_type'   : bcond['function_type']}
        #
        #                d_bconds2[btype][iphysical] = bcond2
        #    d_bconds = d_bconds2
        
    return d_params, d_mesh, d_bconds, d_parallel


def read_config(filename, irank=0):
    """ Reads simulation parameters from configuration (*.conf.in) file.

	Inputs
	------
	    filename: str
	        path to problem config file.
	    irank: int
	        my MPI rank id.
	Outputs
	-------
            parameters: dict
                contains processed problem parameters.
    """

    # Removes the ".in" suffix if present
    if filename[-3:] == '.in': filename = filename[:-8]

    # Reads the ".conf.in" file
    with open(filename+'.conf.in','r',errors='ignore') as confin:

        casename = os.path.basename(filename)

        if irank == 0: print('Reading config file: '+casename+'.conf.in')

        # Initializations
        parameters = dict()
        parameters['path_inputs']              = os.path.dirname(filename)
        parameters['casename']                 = casename
        parameters['l_problems']               = [] 
        parameters['l_sets']                   = [] 
        parameters['l_fields']                 = [] 
        parameters['l_variables_in_volume']    = [] 
        parameters['l_variables_at_sets']      = [] 
        parameters['l_variables_at_physicals'] = [] 
        parameters['l_witness_points']         = [] 
        parameters['l_fields_witness_points']  = [] 
        # Default values
        parameters['nvariables_in_volume']     = 0
        parameters['nvariables_at_sets']       = 0
        parameters['nvariables_at_physicals']  = 0
        parameters['nwitness_points']          = 0
        parameters['nfields_witness_points']   = 0
        parameters['nlevels_interp']           = 0
        parameters['interp_order']             = 1
        parameters['support_size']             = 0.0        
        parameters['ndump_restart']            = 0
        parameters['flag_load_restart']        = False
        parameters['mesh_divisor']             = 0
        while True:
            line = confin.readline()
            if not line: break
            if not line in ['\n','\r\n']:
                words = line.split()
                begin_section               = True
                begin_sets                  = True
                begin_fields                = True
                begin_subproblems           = True
                begin_outputs_in_volume     = True
                begin_outputs_at_sets       = True
                begin_outputs_at_physicals  = True
                begin_witness_points        = True
                begin_coords_witness_points = True
                begin_fields_witness_points = True

                if words[0] == '$PhysicalParameters':
                    while begin_section:
                        line  = confin.readline()
                        words = line.split()
                        if words[0] == '$EndPhysicalParameters': begin_section = False
                        if words[0] == 'physics'    : parameters['physics']    = words[1]
                        if words[0] == 'nu'         : parameters['nu']         = float(words[1])
                        if words[0] == 'rho'        : parameters['rho']        = float(words[1])
                        if words[0] == 'p0'         : parameters['p0']         = float(words[1])
                        if words[0] == 'ndime'      : parameters['ndime']      = int(words[1])
                        if words[0] == 'material'   : parameters['material']   = words[1][0:7]
                        if words[0] == 'young'      : parameters['young']      = float(words[1])
                        if words[0] == 'poisson'    : parameters['poisson']    = float(words[1])
                        if words[0] == 'constraint' : parameters['constraint'] = float(words[1])

                if words[0] == '$NumericalParameters':
                    while begin_section:
                        words = confin.readline().split()
                        if words[0] == '$EndNumericalParameters': begin_section = False
                        if words[0] == 'time_coupling'          : parameters['time_coupling']         = words[1]
                        if words[0] == 'dt'                     : parameters['dt']                    = float(words[1])
                        if words[0] == 'dtmax'                  : parameters['dtmax']                 = float(words[1])
                        if words[0] == 'cfl'                    : parameters['cfl']                   = float(words[1])
                        if words[0] == 'tinitial'               : parameters['tinitial']              = float(words[1])
                        if words[0] == 'tfinal'                 : parameters['tfinal']                = float(words[1])
                        if words[0] == 'ntmax'                  : parameters['ntmax']                 = int(words[1])
                        if words[0] == 'alpha'                  : parameters['alpha']                 = float(words[1])
                        if words[0] == 'gamma'                  : parameters['gamma']                 = float(words[1])
                        if words[0] == 'time_treatment'         : parameters['time_treatment']        = words[1]
                        if words[0] == 'beta'                   : parameters['beta']                  = float(words[1])
                        if words[0] == 'theta'                  : parameters['theta']                 = float(words[1])
                        if words[0] == 'tolerance'              : parameters['tolerance']             = float(words[1])
                        if words[0] == 'nitermax'               : parameters['nitermax']              = int(words[1])
                        if words[0] == 'flag_lump_mass'         : parameters['flag_lump_mass']        = bool(int(words[1]))
                        if words[0] == 'flag_diff_timesteps'    : parameters['flag_diff_timesteps']   = bool(int(words[1]))
                        if words[0] == 'flag_adapt_timestep'    : parameters['flag_adapt_timestep']   = bool(int(words[1]))
                        if words[0] == 'flag_load_restart'      : parameters['flag_load_restart']   = bool(int(words[1]))
                        if words[0] == 'interp_method'          : parameters['interp_method']         = words[1]
                        if words[0] == 'nlevels_interp'         : parameters['nlevels_interp']        = int(words[1])
                        if words[0] == 'interp_order'           : parameters['interp_order']          = int(words[1])
                        if words[0] == 'support_size'           : parameters['support_size']          = float(words[1]) # Support size radious for RKPM interpolation type 
                        if words[0] == 'influence_radius'       : parameters['influence_radius']      = int(words[1])
                        if words[0] == 'penalty_parameter'      : parameters['penalty_parameter']     = float(words[1])
                        if words[0] == 'penalty_method'         : parameters['penalty_method']        = words[1]
                        if words[0] == 'coupling_method'        : parameters['coupling_method']       = words[1].upper()
                        if words[0] == 'influence_radius'       : parameters['influence_radius']      = int(words[1])
                        if words[0] == 'volume_quadrature'      : parameters['volume_quadrature']     = words[1]
                        if words[0] == 'surface_quadrature'     : parameters['surface_quadrature']    = words[1]
                        if words[0] == 'element_search_method'  : parameters['element_search_method'] = words[1]
                        if words[0] == 'nneighbors'             : parameters['nneighbors']            = int(words[1])
                        if words[0] == 'mesh_divisor'           : parameters['mesh_divisor']          = int(words[1])
                    if not('nitermax'  in parameters): parameters['nitermax']  = 1
                    if not('tolerance' in parameters): parameters['tolerance'] = 1.0e-13 

                if words[0] == '$InitialConditions':
                    parameters['initial_conditions'] = dict()
                    while begin_section:
                        words = confin.readline().split()
                        if words[0] == '$EndInitialConditions' : begin_section = False
                        if words[0] == 'type'             : parameters['initial_conditions']['type']        = words[1]
                        if words[0] == 'nparameters'      : parameters['initial_conditions']['nparameters'] = int(words[1])
                        if words[0] == 'parameters'       : parameters['initial_conditions']['parameters']  = \
                            [float(words[i+1]) for i in range(parameters['initial_conditions']['nparameters'])]
                        if words[0] == 'center_of_mass'   : parameters['initial_conditions']['center_of_mass']  = \
                            [float(words[i+1]) for i in range(parameters['ndime'])]
                        if words[0] == 'velocity'         : parameters['initial_conditions']['velocity']  = \
                            [float(words[i+1]) for i in range(parameters['ndime'])]
                        if words[0] == 'acceleration'     : parameters['initial_conditions']['acceleration']  = \
                            [float(words[i+1]) for i in range(parameters['ndime'])]
                        if words[0] == 'angle'            : parameters['initial_conditions']['angle'] = float(words[1])
                        if words[0] == 'ang_velocity'     : parameters['initial_conditions']['ang_velocity'] = float(words[1])
                        if words[0] == 'ang_acceleration' : parameters['initial_conditions']['ang_acceleration'] = float(words[1])

                if words[0] == '$Sources':
                    if len(words) > 1:
                        nsources = int(words[1])
                    else:
                        nsources = 0
                    if irank == 0: print('Reading sources...')
                    nsource = 0
                    d_sources = {}

                    while True:
                        line = confin.readline()
                        if not line.strip(): continue 
                        words = line.split()
                        if words[0] == '$EndSources': break
                        
                        if words[0] == '$Variable':
                            if len(words) > 1: name = words[1]
                            nsource += 1
                            d_sources[name] = {}
                            while True:
                                line = confin.readline()
                                if not line.strip(): continue 
                                words = line.split()
                                if words[0] == '$EndVariable': break
                                if words[0] == 'type'        : d_sources[name]['type']        = words[1]
                                if words[0] == 'nparameters' : d_sources[name]['nparameters'] = int(words[1])
                                if words[0] == 'parameters'  : d_sources[name]['parameters']  = \
                                    [float(words[i+1]) for i in range(d_sources[name]['nparameters'])]
                                if words[0] == 'force'       : d_sources[name]['force']  = \
                                    [float(words[i+1]) for i in range(parameters['ndime'])]
                                if words[0] == 'torque'      : d_sources[name]['torque'] = float(words[1])
                    parameters['sources'] = d_sources
                    if (nsource !=  nsources):
                        print('Error! nsources in conf.in is not consistent with list of sources.')
                        exit()

                if words[0] == '$Inputs':
                    while begin_section:
                        words = confin.readline().split()
                        if words[0] == '$EndInputs': begin_section                 = False
                        if words[0] == '$SubProblems' :
                            while begin_subproblems:
                                words = confin.readline().split()
                                if words[0] == '$EndSubProblems':
                                    begin_subproblems = False
                                    parameters['nproblems'] = len(parameters['l_problems'])
                                else:
                                    parameters['l_problems'].append(words[0])
                        if words[0] == '$Sets' :
                            while begin_sets:
                                words = confin.readline().split()
                                if words[0] == '$EndSets':
                                    begin_sets = False
                                    parameters['nsets'] = len(parameters['l_sets'])
                                else:
                                    parameters['l_sets'].append(words[0])
                                    print('To-do! Implement sets.')
                        if words[0] == '$Fields' :
                            while begin_fields:
                                words = confin.readline().split()
                                if words[0] == '$EndFields':
                                    begin_fields = False
                                    parameters['nfields'] = len(parameters['l_fields'])
                                else:
                                    parameters['l_fields'].append(words[0])
                                    print('To-do! Implement fields.')

                if words[0] == '$Outputs':
                    while begin_section:
                        words = confin.readline().split()
                        if words[0] == '$EndOutputs': begin_section                       = False
                        if words[0] == 'path_outs'        : parameters['path_outs']       = words[1]
                        if words[0] == 'output_datatype'  : parameters['output_datatype'] = words[1]
                        if words[0] == 'ndump'            : parameters['ndump']           = int(words[1])
                        if words[0] == 'ndump_restart'    : parameters['ndump_restart']   = int(words[1])
                        if words[0] == '$InVolume' :
                            while begin_outputs_in_volume:
                                words = confin.readline().split()
                                if words[0] == '$EndInVolume':
                                    begin_outputs_in_volume = False
                                    parameters['nvariables_in_volume'] = len(parameters['l_variables_in_volume'])
                                else:
                                    parameters['l_variables_in_volume'].append(words[0])
                        if words[0] == '$AtSets' :
                            while begin_outputs_at_sets:
                                words = confin.readline().split()
                                if words[0] == '$EndAtSets':
                                    begin_outputs_at_sets = False
                                    parameters['nvariables_at_sets'] = len(parameters['l_variables_at_sets'])
                                else:
                                    parameters['l_variables_at_sets'].append(words[0])
                                    print('Error! Implement variables at sets.')
                        if words[0] == '$AtPhysicalGroups' :
                            while begin_outputs_at_physicals:
                                words = confin.readline().split()
                                if words[0] == '$EndAtPhysicalGroups':
                                    begin_outputs_at_physicals = False
                                    parameters['nvariables_at_physicals'] = len(parameters['l_variables_at_physicals'])
                                else:
                                    parameters['l_variables_at_physicals'].append(words[0])
                        if words[0] == '$WitnessPoints' :
                            while begin_witness_points:
                                words = confin.readline().split()
                                if words[0] == '$EndWitnessPoints':
                                    begin_witness_points = False
                                if words[0] == '$Coordinates':
                                    while begin_coords_witness_points:
                                        words = confin.readline().split()
                                        if words[0] == '$EndCoordinates':
                                            begin_coords_witness_points = False
                                            parameters['nwitness_points'] = len(parameters['l_witness_points'])
                                        else:
                                            parameters['l_witness_points'].append([float(words[i]) for i in range(parameters['ndime'])])
                                if words[0] == '$Fields':
                                    while begin_fields_witness_points:
                                        words = confin.readline().split()
                                        if words[0] == '$EndFields':
                                            begin_fields_witness_points = False
                                            parameters['nfields_witness_points'] = len(parameters['l_fields_witness_points'])
                                        else:
                                            parameters['l_fields_witness_points'].append(words[0])

        # Set some parameters to default values if not defined in config file
        l_optional_keys = ['coupling_method',
                           'penalty_method']

        for key in l_optional_keys:
            if key not in parameters: parameters[key] = None

        if 'nneighbors' not in parameters: parameters['nneighbors'] = 1

    return parameters



def read_mesh(filename, d_params, irank=0):
    """ Reads mesh with Gmsh 2.2 format.
        
        Inputs
        ------
            filename: str
                filename of mesh
            irank: int
                MPI rank ID
        Outputs
        -------
            d_mesh: dict
                contains mesh data
            d_parallel: dict
		constains nodal exchange arrays.
    """

    ndime        = d_params['ndime']
    is_parallel  = d_params['is_parallel']
    mesh_divisor = d_params['mesh_divisor']

    # Get mesh filename (ie: "casename.msh")
    if filename[-3:] == '.in': filename = filename[:-8]
    casename = os.path.basename(filename)
    filename = filename+'.msh'

    
    if irank == 0: print('Reading mesh file: '+filename)

    # Read Gmsh file using API and save to gmsh object
    gmsh.initialize()
    # Disable all messages on the terminal
    gmsh.option.setNumber('General.Terminal', 0)
    gmsh.open(filename)

    # Refine mesh using Gmsh's divisor
    for irefine in range( mesh_divisor ):
        gmsh.model.mesh.refine()
    
    # If the environment is parallel then perform the partion and build the corresponding nodal exchange arrays
    d_parallel = {}
    if is_parallel == True:
        d_parallel = partition.build_parallel_data(ndime)

    o_mesh = gmsh.model


    # Get number of nodes
    # ------------------------------------------------------------------------------------------------ #
    # *TODO* ACA HAY UN PROBLEMA CON LEER CTYPES DESDE NUMPY
    #   Probar con https://stackoverflow.com/questions/4355524/getting-data-from-ctypes-array-into-numpy
    l_inodes,l_node_coords,_ = o_mesh.mesh.getNodes()
    # Transform to Python indexing (starts from 0)
    l_inodes = l_inodes-1
    # ------------------------------------------------------------------------------------------------ #
    nnode = len(l_inodes)
    l_node_coords = l_node_coords.reshape((nnode,-1))

    # Get unique element types
    l_elem_types = np.unique(o_mesh.mesh.getElements()[0])

    # Get dimension of manifold
    ndime = 0
    for elem_type in l_elem_types:
        _,elem_dime,_,_,_,_ = o_mesh.mesh.getElementProperties(elem_type)
        ndime = max([ndime,elem_dime])

    nelem_surf     = 0 
    elem_type_surf = []
    # Get number of volume and surface elements
    for elem_type in l_elem_types:
        # Get properties of current element type
        elem_name,elem_dime,_,_,_,_ = o_mesh.mesh.getElementProperties(elem_type)

        # Get number of volume elements
        if elem_dime == ndime:
            l_ielems,_    = o_mesh.mesh.getElementsByType(elem_type)
            nelem_vol     = len(l_ielems)
            elem_type_vol = elem_type
        # Get number of surface elements
        elif elem_dime > 0:
            l_ielems,_     = o_mesh.mesh.getElementsByType(elem_type)
            nelem_surf     = len(l_ielems)
            elem_type_surf = elem_type

    d_mesh = {'name'           : casename,
              'gmsh'           : o_mesh,
              'ndime'          : ndime,
              'nnode'          : nnode,
              'l_node_coords'  : l_node_coords,
              'nelem'          : nelem_vol,
              'elem_type'      : elem_type_vol,
              'nelem_surf'     : nelem_surf,
              'elem_type_surf' : elem_type_surf}

    return d_mesh, d_parallel
        

def read_boundary_conditions(filename, d_mesh, irank=0):
    """ Reads boundary conditions from *.boun.in file.
    
        Inputs
        ------
            filename: str
                filename of mesh
            irank: int
                MPI rank ID
        Outputs
        -------
            d_bconds: dict
                contains boundary conditions data
    """

    if irank == 0: print('Reading boundary conditions file: '+filename+'.boun.in')

    # Open boundary conditions file
    if filename[-3:] == '.in': filename = filename[:-8]
    filename    = filename+'.boun.in'
    path_inputs = os.path.dirname(filename)
    nnode = d_mesh['nnode']
    ndime = d_mesh['ndime']
 
    # Read boundary conditions file
    with open(filename, 'r') as f:

        # Initialize boundary conditions dict
        #d_bconds = { 'nodes'    : {},
        #             'elements' : {},
        #             'zero'     : {} }
        #             'forces'   : {} }

        # Read line-by-line
        while True:
            line = f.readline()
            if not line: break
            if not line.strip(): continue 
            words = line.split()

            # Read equation names
            if words[0] == '$Equations':
                iequation   = 0
                l_equations = []                
                while True:
                    words = f.readline().split()                    
                    if words[0] == '$EndEquations': break
                    
                    iequation            = iequation + 1
                    l_equations.append(words[0])
                nequations = iequation
                l_current_equations = np.zeros(nequations)                    
            
            # Read boundary conditions section
            if words[0] == '$BoundaryConditions':
                # Initialize boundary conditions dict                
                d_bconds = {}                
                while True:
                    words = f.readline().split()
                    
                    if words[0] == '$EndBoundaryConditions': break
                    
                    # Read variables where conditions are imposed (as many as variables )
                    for iequation in range(nequations):
                        if words[0].lower() == '$'+l_equations[iequation]:
                            l_current_equations[iequation] = 1
                            d_bconds[l_equations[iequation]] = {'nodes': {}}                            
                            
                     # Read nodal conditions imposed in a determined equation/equations                           
                    if words[0] == '$Nodes':
                        while True:
                            line = f.readline()
                            if not line.strip(): continue 
                            words = line.split()
                            if words[0] == '$EndNodes': break
                            nwords             = len(words)
                            boundary_condition = {}
                            iphysical          = int(words[0])
                            ndofs              = len(words[1])
                            boundary_condition['iphysical'] = iphysical
                            boundary_condition['types']     = np.array([int(words[1][i])  for i in range(ndofs)])
                            boundary_condition['factors']   = np.array([float(words[2+i]) for i in range(ndofs)])
                            if nwords > ndofs+2:
                                boundary_condition['function_number'] =  int(words[2+ndofs])

                            for iequation in range(nequations):
                                if l_current_equations[iequation] == 1:                                    
                                    d_bconds[l_equations[iequation]]['nodes'][iphysical] = boundary_condition

                    # Read equation where conditions are not imposed anymore
                    for iequation in range(nequations):
                        if words[0].lower() == '$end'+l_equations[iequation]:
                            l_current_equations[iequation] = 0
                                                                                
#                    if words[0] == '$Elements':
#                        while True:
#                            line = f.readline()
#                            if not line.strip(): continue 
#                            words = line.split()
#                            if words[0] == '$EndElements': break
#                            nwords             = len(words)
#                            boundary_condition = {}
#                            iphysical          = int(words[0])
#                            ndofs              = len(words[1])
#                            boundary_condition['iphysical'] = iphysical
#                            boundary_condition['types']     = np.array([int(words[1][i])  for i in range(ndofs)])
#                            boundary_condition['factors']   = np.array([float(words[2+i]) for i in range(ndofs)])
#                            if nwords > ndofs+2:
#                                boundary_condition['function_number'] =  int(words[2+ndofs])
#                            d_bconds['elements'][iphysical] = boundary_condition
#                    if words[0] == '$Forces':
#                        while True:
#                            line = f.readline()
#                            if not line.strip(): continue 
#                            words = line.split()
#                            if words[0] == '$EndForces': break
#                            nwords             = len(words)
#                            boundary_condition = {}
#                            iphysical          = int(words[0])
#                            ndofs              = len(words[1])
#                            boundary_condition['iphysical'] = iphysical
#                            boundary_condition['types']     = np.array([int(words[1][i])  for i in range(ndofs)])
#                            boundary_condition['factors']   = np.array([float(words[2+i]) for i in range(ndofs)])
#                            if nwords > ndofs+2:
#                                boundary_condition['function_number'] =  int(words[2+ndofs])
#                            d_bconds['forces'][iphysical] = boundary_condition

            # Read functions 
            if words[0] == '$Functions':
                if len(words) > 1: nfunctions = int(words[1])
                
                if irank == 0: print('Reading functions...')
                nfunction = 0
                d_functions = {}

                while True:
                    line = f.readline()
                    if not line.strip(): continue 
                    words = line.split()
                    if words[0] == '$EndFunctions': break
                    
                    if words[0] == '$Function':
                        if len(words) > 1: function_number = int(words[1])
                        nfunction += 1
                        while True:
                            line = f.readline()
                            if not line.strip(): continue 
                            words = line.split()
                            if words[0] == '$EndFunction': break
                            function = {}
                            if words[0] == 'function_number':
                                function_number = int(words[1])
                            elif words[0] == 'include':
                                d_functions[(function_number)] = read_function_from_file(words[1:], path_inputs)
                            elif words[0] == 'from_coupling':
                                d_functions[(function_number)] = {'function' : [],
                                                                  'type'     : 'from_coupling'}
                            else:
                                d_functions[(function_number)] = read_space_time_function(words[0:], path_inputs)
                if (nfunction !=  nfunctions):
                    print('Error! nfunctions in boun.in is not consistent with list of functions.')
                    exit()
                    
    # Store corresponding functions in BCs dict (using mutability property of dicts)
    for equation in d_bconds:    
        for btype in d_bconds[equation]:
            for iphysical, boundary_condition in d_bconds[equation][btype].items():
                function_number = boundary_condition['function_number']
                if function_number != 0:
                    boundary_condition['function']      = d_functions[(function_number)]['function']
                    boundary_condition['function_type'] = d_functions[(function_number)]['type']
                else:
                    boundary_condition['function']      = None
                    boundary_condition['function_type'] = 'constant'
                    
#    # Translate boundary conditions to nodal arrays
#    d_bcs = { 'fixity'           : np.zeros((nnode, ndime), bool),
#              'dirichlet_values' : np.zeros((nnode, ndime), float),
#              'neumann_values'   : np.zeros((nnode, ndime), float),
#              'function_numbers' : np.zeros((nnode, ndime), int)    }
#    for btype in d_bconds:
#        for iphysical, bcond in d_bconds[btype].items():
#            fixity = bcond['types']
#            values = bcond['factors']
#            for ndim in range(ndime-1,-1,-1):
#                l_inodes = d_mesh['gmsh'].mesh.getNodesForPhysicalGroup(ndim, iphysical)[0]
#                if btype == 'nodes':
#                    d_bcs['fixity'][l_inodes,:]           = fixity
#                    d_bcs['dirichlet_values'][l_inodes,:] = values
#                elif btype == 'elements':
#                    d_bcs['neumann_values'][l_inodes,:]   = values
#                elif btype == 'forces':
#                    d_bcs['forces'][l_inodes,:]           = values 
#                elif 'function_number' in bcond:
#                    d_bcs['function_numbers'][l_inodes] = bcond['function_number']

    return d_bconds


def read_function_from_file(function_strings, path_inputs):
    """ Reads external function used to prescribe
        dynamic boundary conditions.
        
        Inputs
        ------
            function_strings: str
                string with line where function is defined in .boun.in file
            path_inputs: str
                path to .boun.in file
        Outputs
        -------
            d_function: dict
                contains space-time function
    """

    # Read function file
    filename = os.path.join(path_inputs,function_strings[0])
    with open(filename, 'r') as f:
    
        # Read time-series
        while True:
            line = f.readline()
            if not line: break
            if not line.strip(): continue
            words = line.split()
    
            if words[0] == 'time':
                npoint = 0
                data = []
                while True:
                    line = f.readline()
                    if not line: break
                    words  = line.split()
                    if (npoint==0): ndofs = len(words)-1
                    npoint += 1
                    data.append([float(words[i]) for i in range(ndofs+1)])
    
    data = np.array(data)
    
    # Build time-series interpolator
    function = []
    for idof in range(ndofs):
        function.append( interp1d(data[:,0], data[:,idof+1]) )

    # Return as dict
    d_function = {'function' : function,
                  'type'     : 'time_function'}
    return d_function


def read_space_time_function(function_strings, path_inputs):
    """ Reads externally defined space-time function used
        to prescribe dynamic boundary conditions.
    
        Inputs
        ------
            function_strings: str
                string with line where function is defined in .boun.in file
            path_inputs: str
                path to .boun.in file
        Outputs
        -------
            d_function: dict
                contains space-time function
    """

    list_function_expressions = function_strings 
    ndofs = len(list_function_expressions)
    
    # --- this should be changed for a for loop, for some reason it doesn't work --- #
    function = []
    for idof in range(ndofs):
        function.append( lambda t,x,y,z: eval(list_function_expressions[0]+'+0.0*x+0.0*y+0.0*z') )
    # ------------------------------------------------------------------------------ #

    # Find function type (time function or space-time function)
    l_coords      = ['x','y','z']
    function_type = 'time_function'
    for expr in list_function_expressions:
        sp_expr        = ast.parse(expr)
        l_vars_in_expr = [node.id for node in ast.walk(sp_expr) if type(node) is ast.Name]
        if any([coord in l_vars_in_expr for coord in l_coords]):
            function_type = 'space-time_function'

    # Return as dict
    d_function = {'function' : function,
                  'type'     : function_type}
    return d_function


def print_header(irank=0):
    """ Prints program header. """
    if irank == 0:
        #print('+------------------------------------------------------------------------------+')
        #print('!                                  FsiPy                                       !')
        #print('+------------------------------------------------------------------------------+')
        ascii_art = """

+------------------------------------------------------------------------------+
|                                                                              |
|                 $$$$$$$$\           $$\ $$$$$$$\                             |
|                 $$  _____|          \__|$$  __$$\                            |
|                 $$ |       $$$$$$$\ $$\ $$ |  $$ |$$\   $$\                  |
|                 $$$$$\    $$  _____|$$ |$$$$$$$  |$$ |  $$ |                 |
|                 $$  __|   \$$$$$$\  $$ |$$  ____/ $$ |  $$ |                 |
|                 $$ |       \____$$\ $$ |$$ |      $$ |  $$ |                 |
|                 $$ |      $$$$$$$  |$$ |$$ |      \$$$$$$$ |                 |
|                 \__|      \_______/ \__|\__|       \____$$ |                 |
|                                                   $$\   $$ |                 |
|                                                   \$$$$$$  |                 |
|                                                    \______/                  |
|   Authors:     David Oks & Cristóbal Samaniego                               |
|   Institution: Barcelona Supercomputing Center                               |
|   Contact:     david.oks@bsc.es & cristobal.samaniego@bsc.es                 |
|   Revision:    October 2022                                                  |
|                                                                              |
+------------------------------------------------------------------------------+

        """
        print(ascii_art)
