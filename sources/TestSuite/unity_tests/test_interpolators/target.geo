Include "params.geo";

x0 = -ls/2;
y0 = -ls/2;

// Points
Point(1) = {x0,    y0,    0, hs};
Point(2) = {x0+ls, y0,    0, hs};
Point(3) = {x0+ls, y0+ls, 0, hs};
Point(4) = {x0,    y0+ls, 0, hs};

// Lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// Surfaces
Curve Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};

// Tags
Physical Point("sw")       = {1};
Physical Point("se")       = {2};
Physical Point("ne")       = {3};
Physical Point("nw")       = {4};
Physical Curve("s")        = {1};
Physical Curve("e")        = {2};
Physical Curve("n")        = {3};
Physical Curve("w")        = {4};
Physical Surface("domain") = {1};
