from   mpi4py       import MPI
import numpy        as np
import numpy.linalg as la
import gmsh
import os, sys, inspect
import unittest
import pytest

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(currentdir)))
sys.path.insert(0, parentdir)
from Main import *
from aux import *
import pprocess as pp



class interpolation_unittest(unittest.TestCase):
    """ Unit-test for interpolators: calculates interpolation error on an analytic field. """

    # parameters
    _filename     =   os.path.join(currentdir,'test_interpolation.conf.in');
    _size         =   MPI.COMM_WORLD.Get_size();
    _save_outputs =   False
    _tolerance    =   1e-10
    _ftype        =   'gauss'
    _params_intrp = { }
    _params_field = { 'mu'    : [.0, .0] ,
                      'sigma' : [1., 1.] }


    @pytest.mark.skipif(_size != 1, reason='test is sequential')    
    def test_FE_interpolation(self):

        self._error_ref    =   1.633152754494861e-05
        self._params_intrp = { 'interp_method'  : 'FEM' }

        assert self.run_test()


    @pytest.mark.skipif(_size != 1, reason='test is sequential')    
    def test_Kriging_interpolation(self):

        self._error_ref    =   1.0603976705283139e-05
        self._params_intrp = { 'interp_method'  : 'KRIGING',
                               'nlevels_interp' :  1       }

        assert self.run_test()


    @pytest.mark.skipif(_size != 1, reason='test is sequential')    
    def test_RKPM_interpolation(self):

        self._error_ref    =   1.651818852385552e-05
        self._params_intrp = { 'interp_method'  : 'RKPM' ,
                               'nlevels_interp' : 1      ,
                               'support_size'   : 0.1  }

        assert self.run_test() 


    def setup_problem(self):
        """ Sets up problem. """

        # reads inputs
        d_params, d_mesh, d_bconds, d_parallel = parser.read_inputs(self._filename, self._size)
        # modifies interpolation parameters to those setup at header
        d_params = self.change_parameters(d_params)
        # builds problem
        self._problem = build_problem(d_params, d_mesh, d_bconds, d_parallel, self._size)
        # runs simulation to setup variables
        self._problem.run_simulation()


    def change_parameters(self, d_params):
        """ Modifies parameters to those setup at the header of this script. """

        for key in self._params_intrp:
            d_params[key] = self._params_intrp[key]

        return d_params


    def do_interpolation(self):
        """ Computes interpolation. """

        # load parameters
        problem       = self._problem
        name_s        = problem._background
        name_t        = problem._patch
        source        = problem.get_problem_from_coupling(0, 'source')
        target        = problem.get_problem_from_coupling(0, 'target')
        coords_s      = source._o_mesh._l_node_coords
        coords_t      = target._o_mesh._l_node_coords

        # calculate exact field in source mesh
        field_s_exact = calculate_analytic_field( coords_s, self._ftype, self._params_field )
        ndime         = field_s_exact.ndim

        # interpolate field to target mesh
        field_t_intrp = problem.interpolate( ndime, name_s, name_t, field_s_exact )

        # calculate exact field in target mesh
        field_t_exact = calculate_analytic_field( coords_t, self._ftype, self._params_field )
        ndime         = field_s_exact.ndim

        # save interpolated and exact fields in target mesh
        self._intrp   = field_t_intrp
        self._exact   = field_t_exact

        # save to vtk
        if self._save_outputs:
            pp.save2vtk('./',
                        'source',
                        source._o_mesh._l_node_coords,
                        source._o_mesh._l_elems,
                        source._o_mesh._elem_type,
                        {'exact':field_s_exact},
                        ['exact'],
                        source._tcount,
                        source._time)
            pp.save2vtk('./',
                        'target',
                        target._o_mesh._l_node_coords,
                        target._o_mesh._l_elems,
                        target._o_mesh._elem_type,
                        {'intrp':self._intrp,'exact':self._exact},
                        ['intrp','exact'],
                        target._tcount,
                        target._time)


    def run_test(self):
        """ Tests interpolation method. """

        # setup problem
        self.setup_problem()

        # do interpolation
        self.do_interpolation()

        # calculate error as nodal mean of L2 norm between exact & interpolated fields
        self._error       = la.norm( self._intrp - self._exact ) / ( len( self._exact ) )
        print('Error          (mean nodal L2 norm):', self._error)
        
        # check if error is the same as reference value
        passed_test = np.abs( (self._error - self._error_ref) / self._error_ref ) < self._tolerance

        return passed_test


if __name__ == "__main__":
    """ To run as stand-alone function. """

    # setup object
    test = interpolation_unittest()

    # run tests for all interpolators
    test.test_FE_interpolation()
    test.test_Kriging_interpolation()
    test.test_RKPM_interpolation()
