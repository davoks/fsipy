// Fluid domain for Turek & Hron's CFD3 benchmark

// Fluid domain
L  = 1.5;
H  = 0.41;
xc = 0.2;
yc = 0.2;
r  = 0.05;

// Solid domain
l   = 0.35;
h   = 0.02;
drb = 0.04898979485566357;
xb  = xc+drb;
yb  = yc-h/2;

// Characteristic sizes
lout = 0.04;
lin  = 0.04;
lb   = 0.02;

//// Number of points
//Nx  = 101;
//Ny  = 18;
//Nc1 = 20;
//Nc2 = 4;

// Points
Point(1) = {0.0,  0.0,    0, lin};
Point(2) = {L,    0.0,    0, lout};
Point(3) = {L,    H,      0, lout};
Point(4) = {0.0,  H,      0, lin};
Point(5)  = {xc,   yc,     0, lb};
Point(6)  = {xb,   yc+h/2, 0, lb};
Point(7)  = {xc-r, yc,     0, lb};
Point(8)  = {xb,   yc-h/2, 0, lb};
Point(9)  = {xb+l, yc-h/2, 0, lb};
Point(10) = {xb+l, yc+h/2, 0, lb};

// Lines
Line(1)   = {1, 2};
Line(2)   = {2, 3};
Line(3)   = {3, 4};
Line(4)   = {4, 1};
Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 8};
Line(7)   = {8, 9};
Line(8)   = {9, 10};
Line(9)   = {10, 6};

// Surfaces
Curve Loop(1) = {1, 2, 3, 4};
Curve Loop(2) = {5, 6, 7, 8, 9};
Plane Surface(1) = {1,2};

// Tags
Physical Point("channel_sw") = {1};
Physical Point("channel_se") = {2};
Physical Point("channel_ne") = {3};
Physical Point("channel_nw") = {4};
Physical Point("beam_nw")    = {6};
Physical Point("beam_sw")    = {8};
Physical Point("beam_se")    = {9};
Physical Point("beam_ne")    = {10};
Physical Curve("channel_s")  = {1};
Physical Curve("channel_e")  = {2};
Physical Curve("channel_n")  = {3};
Physical Curve("channel_w")  = {4};
Physical Curve("flag")       = {5, 6, 7, 8, 9};
Physical Surface("domain")   = {1};
