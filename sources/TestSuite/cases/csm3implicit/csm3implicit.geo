// Solid domain for Turek and Hron's elastic tail benchmark

// Fluid domain
L  = 2.5;
H  = 0.41;
xc = 0.2;
yc = 0.2;
r  = 0.05;

// Solid domain
l   = 0.35;
h   = 0.02;
drb = 0.04898979485566357;
xb  = xc+drb;
yb  = yc-h/2;

// Number of points
Nx = 71;
Ny = 5;

// Points
Point(1) = {xb,   yb,   0, 1.0};
Point(2) = {xb+l, yb,   0, 1.0};
Point(3) = {xb+l, yb+h, 0, 1.0};
Point(4) = {xb,   yb+h, 0, 1.0};

// Lines
Line(1) = {1, 2}; Transfinite Line {1} = Nx;
Line(2) = {2, 3}; Transfinite Line {2} = Ny;
Line(3) = {3, 4}; Transfinite Line {3} = Nx;
Line(4) = {4, 1}; Transfinite Line {4} = Ny;

// Surfaces
Curve Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};
Transfinite Surface "*";
Recombine Surface "*"; 

// Tags
Physical Point("sw") = {1};
Physical Point("se") = {2};
Physical Point("ne") = {3};
Physical Point("nw") = {4};
Physical Curve("south") = {1};
Physical Curve("east") = {2};
Physical Curve("north") = {3};
Physical Curve("west") = {4};
Physical Surface("domain") = {1};
