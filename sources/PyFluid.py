
"""
    FsiPy Fluid Module
    ==================
"""

# External libraries
import numpy         as np
import numpy.linalg  as la
from   mpi4py        import MPI

# Internal libraries
import PyKernel      as PyKernel
import FortranFluid  as FortranFluid
import aux           as aux
from   PyParameters  import *


class Fluid(PyKernel.Problem):
    """ Fluid class in Python. """

    # Initialize object 
    def __init__(self, d_params, d_mesh, d_bconds, d_parallel, rank):
        """ Initializes Problem object. """

        # Physics module name
        d_params['module_name']        = 'FortranFluid'
        # Number of past timesteps to be saved
        d_params['npast']              = 4

        # Names of variables
        self._l_variables_names        = ['fractional-velocity',
                                          'pressure',
                                          'velocity',
                                          'stabilization']
        # Dimensionality of variables ['vector'|'scalar']
        self._l_variables_dims         = ['vector',
                                          'scalar',
                                          'vector',
                                          'vector']
        # Type of variables ['primary'|'secondary']
        self._l_variables_types        = ['primary',
                                          'primary',
                                          'primary',
                                          'primary']
        # Names of equations
        self._l_equation_names         = ['velocity_prediction',
                                          'pressure_solution',
                                          'velocity_correction',
                                          'stabilization_solution']
        # Primary variables involved in each equation
        self._l_variables_in_equations = [self._l_variables_names[0],
                                          self._l_variables_names[1],
                                          self._l_variables_names[2],
                                          self._l_variables_names[3]]
        # Variables involved in boundary conditions
        # Allow us to impose values directly on a different variable thtan the primary one (see PySolid)        
        self._d_dofs_in_bconds        = {self._l_equation_names[0]: self._l_variables_names[0],
                                         self._l_equation_names[1]: self._l_variables_names[1],
                                         self._l_equation_names[2]: self._l_variables_names[2],
                                         self._l_equation_names[3]: self._l_variables_names[3]}        
        self._l_tasks_begin_timestep  = ['update_time',
                                         'update_boundary_conditions'];
        self._l_tasks_begin_equation  = ['update_aux_iterative_variables'];
        self._l_tasks_begin_iteration = [];
        self._l_tasks_do_iteration    = ['assemble_algebraic_system',
                                         'impose_boundary_conditions_on_algebraic_system',
                                         'solve_algebraic_system']
        self._l_tasks_end_iteration   = ['compute_iteration_error'];
        self._l_tasks_end_equation    = [];
        self._l_tasks_end_timestep    = ['calculate_forces_from_stresses',
                                         'update_aux_time_variables',
                                         'update_time_variables',
                                         'postprocess'];

        # Flags for dynamic assembly of LHS in each equation
        if abs(d_params['theta']) < 1e-10:
            self._l_flags_dynamic_lhs = [False, False, False, False]
        else:
            self._l_flags_dynamic_lhs = [True, False, False, False]
 
        # Flags for dynamic lumping of LHS in correponding each equation
        if d_params['flag_lump_mass'] == True:
            self._l_flags_for_lumping = [True, False, True, True]
        else:
            self._l_flags_for_lumping = [False, False, False, False]
 
       
        # Initialize Finite Element Problem object
        PyKernel.Problem.__init__(self,
                                  d_params,
                                  d_mesh,
                                  d_bconds,
                                  d_parallel,
                                  rank)



    def initialize_fortran_module(self):
        """ Initializes corresponding Fortran Module. """

        module_name = self._d_params['module_name']
        str_expr = module_name+'.'+module_name.lower() \
                 + '.initialize('                      \
                 + 'self._o_mesh._l_elems, '           \
                 + 'self._o_mesh._gauss_weights, '     \
                 + 'self._o_mesh._gp_jacobians, '      \
                 + 'self._o_mesh._gp_inv_jacobians, '  \
                 + 'self._o_mesh._gp_det_jacobians, '  \
                 + 'self._o_mesh._gp_coords, '         \
                 + 'self._o_mesh._gp_shape, '          \
                 + 'self._o_mesh._gp_dshape, '         \
                 + 'self._o_mesh._gp_volume, '         \
                 + 'self._tau, '                       \
                 + 'self._rho, '                       \
                 + 'self._nu, '                        \
                 + 'self._d_params[\'material\'], '    \
                 + 'self._d_params[\'dt\'], '          \
                 + 'self._d_params[\'gamma\'], '       \
                 + 'self._d_params[\'theta\'], '       \
                 + 'self._o_mesh._nnode, '             \
                 + 'self._ndofs, '                     \
                 + 'self._npast, '                     \
                 + 'self._ndime, '                     \
                 + 'self._o_mesh._nelem, '             \
                 + 'self._o_mesh._nnode_elem, '        \
                 + 'self._o_mesh._ngaus, '             \
                 + 'self._o_mesh._nbasis) '
        exec(str_expr)

        # Load required Finite Element arrays and matrices
        self.get_mass_matrix()



    def initialize_material_properties(self):
        """ Initializes material properties. 

            Outputs
            -------
                self._rho : numpy array of dims (nelem,ngaus)
                    fluid mass density at each gauss point.
                self._nu : numpy array of dims (nelem,ngaus)
                    fluid kinematic viscosity at each gauss point.
        """

        # Save mass density and kinematic viscosity at each gauss point
        self._rho = np.zeros((self._nelem,self._o_mesh._ngaus),float)
        self._nu  = np.zeros((self._nelem,self._o_mesh._ngaus),float)
        for ielem in range(self._nelem):
            for igaus in range(self._o_mesh._ngaus):
                self._rho[ielem,igaus] = self._d_params['rho']
                self._nu[ielem,igaus]  = self._d_params['nu']



    def compute_timestep_size(self):
        """ Calculate critical timestep and intrinsic time
            for SUPG stabilization.

            Outputs
            -------
                self._d_params['dt'] : float
                    time-step size.
                self._d_params['cfl'] : float
                    CFL number ( = dt / dt_critical )
                self._d_params['dt_critical'] : float
                    critical timestep size.
        """

        # For now only static timestep sizes    
        if self._tcount > 0: return

        # Compute global critical timestep and element intrinsic times
        dt_critical = 1e13
        hmin = 1e13
        self._tau = np.zeros((self._nelem),float)
        for ielem in range(self._nelem):
            l_inodes    = self._o_mesh._l_elems[ielem,:]
            h           = self._o_mesh._l_elem_lengths[ielem]
            Uelem       = self._d_variables['velocity'][:,l_inodes,iter_k]
            Umax        = np.amax(np.linalg.norm(Uelem, axis=1))
            nu          = np.mean(self._nu[ielem,:])
            dt_element  = 1.0/(4.0*nu/h**2 + 2.0*Umax/h)
            hmin        = min([hmin, h])

            # SUPG intrinsic time fore each element
            self._tau[ielem] = 0.1 * dt_element

            # Global critical timestep
            dt_critical = min(dt_critical, dt_element)

        # Timestep from critical
        if self._d_params['time_coupling'] == 'from_critical':
            dt  = self._d_params['cfl'] * dt_critical
            cfl = self._d_params['cfl']
        # Fixed timestep
        else:
            dt  = self._d_params['dt']
            cfl = self._d_params['dt'] / dt_critical

        # Save to dict
        self._d_params['dt']          = dt
        self._d_params['cfl']         = cfl
        self._d_params['dt_critical'] = dt_critical
        self._d_params['min_elem_size'] = hmin



    def print_parameters(self):

        if MPI.COMM_WORLD.Get_rank() == 0:        
            print('\nProblem Parameters:')
            print('-------------------\n')
            print('    Physics:             ',self._d_params['physics'])
            print('    Material model:      ',self._d_params['material'])
            print('    Mass density:        ',self._d_params['rho'])
            print('    Kinematic viscosity: ',self._d_params['nu'])
            print('    CFL Number:          ',self._d_params['cfl'])
            print('    Timestep:            ',self._d_params['dt'])
            print('    Gamma:               ',self._d_params['gamma'])
            print('    Theta:               ',self._d_params['theta'])
            print('    Number of elements:  ',self._nelem)
            print('    Number of nodes:     ',self._nnode)
            print('    Minimum element size:',self._d_params['min_elem_size'])
            print('    Volume Quadrature:   ',self._d_params['volume_quadrature'])
            print('    Surface Quadrature:  ',self._d_params['surface_quadrature'])
            print(' ')



#    def calculate_forces_from_stresses(self, flag_cancel_dirichlet_dofs=False):
#        """ Computes forces from fluid stresses in volume.
#
#                    +-
#                F = |  div(sigma) dV   with   sigma = -p*I + mu*[grad(u)+grad(u)^t]
#                   -+
#
#            Output
#            ------
#                stress : numpy array of dims (ndime,nnode)
#                    forces from fluid stresses for each node
#        """
#        #
#        # Initialize variables
#        ngaus      = self._o_mesh._ngaus
#        nnode_elem = self._o_mesh._nnode_elem
#        stress     = np.zeros((self._ndime, self._nnode), float)
#        #
#        # Loop over elements
#        #
#        for ielem in range(self._nelem):
#            # Get nodes of element
#            l_inodes = self._o_mesh._l_elems[ielem,:]
#            # Get velocity and pressure at nodes of volume element
#            elvelocity = np.squeeze(self._d_variables['velocity'][:,l_inodes,iter_k])
#            elpressure = np.squeeze(self._d_variables['pressure'][l_inodes,iter_k])
#            #
#            # Loop over gauss points
#            #
#            elrhs = np.zeros((nnode_elem, self._ndime), float)
#            for igaus in range(ngaus):
#                # Compute laplacian and divergence matrices for gauss point
#                gp_dshape_glo    = self.compute_global_shape_function_derivatives(ielem, igaus)
#                gp_dshape_dshape = self.compute_laplacian_matrix(ielem, igaus, gp_dshape_glo)
#                gp_shape_dshape  = self.compute_divergence_matrix(ielem, igaus, gp_dshape_glo)
#                # Compute viscous stresses at gauss point
#                gprhs = self.compute_viscous_stresses_at_gp(ielem,
#                                                            igaus,
#                                                            elvelocity,
#                                                            elpressure,
#                                                            gp_shape_dshape,
#                                                            gp_dshape_dshape)
#                #
#                # Assemble gauss point contribution to element vector
#                #
#                for inode in range(nnode_elem):
#                    for idime in range(self._ndime):
#                        elrhs[inode,idime] += gprhs[inode,idime] \
#                                            * self._o_mesh._gp_volume[ielem,igaus]
#            #
#            # Assemble element contribution to global vector
#            #
#            for inode_loc, inode in enumerate(l_inodes):
#                for idime in range(self._ndime):
#                    stress[idime,inode] += elrhs[inode_loc,idime]
#
#        self._force = stress
#
#        return stress
#
#
#
#    def compute_viscous_stresses_at_gp(self,
#                                       ielem,
#                                       igaus,
#                                       elvelocity,
#                                       elpressure,
#                                       gp_shape_dshape,
#                                       gp_dshape_dshape):
#        nnode_elem = self._o_mesh._nnode_elem
#        gprhs = np.zeros((nnode_elem, self._ndime), float)
#        for inode in range(nnode_elem):
#            for idime in range(self._ndime):
#                for jnode in range(nnode_elem):
#                    # pressure stresses
#                    gprhs[inode,idime] += -gp_shape_dshape[inode,jnode,idime] \
#                                        *  elpressure[jnode]
#                    # viscous stresses
#                    gprhs[inode,idime] += self._rho[ielem,igaus] * self._nu[ielem,igaus] \
#                                        * gp_dshape_dshape[inode,jnode] \
#                                        * elvelocity[idime,jnode]
#        return gprhs



    def compute_stresses_at_boundaries(self):
        """ Computes stresses at boundary physical groups of problem.

                    +-
                F = |  sigma * n dS   with   sigma = -p*I + mu*[grad(u)+grad(u)^t]
                   -+

            Output
            ------
                stress : numpy array of dims (ndime,nboun)
                    drag (ndime=0) and lift (ndime=1) forces integrated over
                    each boundary identified by its corresponding physical tag
        """
        # ------------------------------------------------------------------------------- #
        # TODO: move this to a generic function in PyKernel for integrating quantities at #
        #       boundaries. PyFluid should only give the gauss-point contribution to the  #
        #       drag and lift forces.                                                     #
        # ------------------------------------------------------------------------------- #
        #
        # Initialize variables
        ngaus_surf     = self._o_mesh._ngaus_surf
        nbasis         = self._o_mesh._nbasis
        elem_type      = self._o_mesh._elem_type
        stress         = np.zeros((self._nbounds, self._ndime), float)
        gp_coords_surf = np.zeros((ngaus_surf,3),float)
        #
        # Loop over boundaries
        #
        for iboun, iphysical in enumerate(self._l_phys_bounds):
            l_ielems_surf = self.get_elements_for_physical_group(self._ndime-1, iphysical)
            #
            # Loop over surface elements
            #
            for ielem_surf in l_ielems_surf:
                # Get coords of boundary gauss points
                gp_coords_surf[:,0:self._ndime] = self._o_mesh._gp_coords_surf[ielem_surf,:,:]
                # Get Gmsh tag of associated volume element
                ielem_gmsh = self._o_mesh.getElementByCoordinates(gp_coords_surf[0,0],
                                                                  gp_coords_surf[0,1],
                                                                  gp_coords_surf[0,2],
                                                                  dim=self._ndime)
                # Transform to FsiPy element indexing
                ielem = int( ielem_gmsh - self._o_mesh._l_ielems_gmsh[0] )
                # Get nodes of volume and surface elements
                l_inodes      = self._o_mesh._l_elems[ielem,:]
                l_inodes_surf = self._o_mesh._l_elems_surf[ielem_surf,:]
                # Get velocity and pressure at nodes of volume element
                elvelocity = np.squeeze(self._d_variables['velocity'][:,l_inodes,iter_k])
                elpressure = np.squeeze(self._d_variables['pressure'][l_inodes,iter_k])
                #
                # Loop over boundary gauss points
                #
                for igaus_surf in range(self._o_mesh._ngaus_surf):
                    # Get isoparametric coords of boundary gauss points
                    coords_local = self._o_mesh.getLocalCoordinatesInElement(ielem_gmsh,
                                                           gp_coords_surf[igaus_surf,0],
                                                           gp_coords_surf[igaus_surf,1],
                                                           gp_coords_surf[igaus_surf,2])
                    # Get volume shape functions (& derivs) evaluated at boundary gauss points
                    _,gp_shape,_ =self._o_mesh.getBasisFunctions(elem_type,coords_local,'Lagrange')
                    _,gp_dshape,_=self._o_mesh.getBasisFunctions(elem_type,coords_local,'GradLagrange')
                    gp_dshape     = gp_dshape.reshape(nbasis, 3)[:,0:self._ndime]
                    # Get jacobian of volume shape functions evaluated at boundary GPs
                    gp_jacobian,_,_ = self._o_mesh.getJacobian(ielem_gmsh, list(coords_local))
                    # Compute inverse
                    gp_inv_jacobian = np.linalg.inv(gp_jacobian.reshape(3,3)[:self._ndime,:self._ndime])
                    # Initialize arrays
                    gp_stress     = np.zeros(self._ndime,float)
                    gp_pressure   = 0.0
                    gra_velocity  = np.zeros((self._ndime,self._ndime),float)
                    gp_dshape_glo = np.zeros((self._o_mesh._nnode_elem,self._ndime),float)
                    # Compute shape function gradients at boundary gauss points
                    for inode in range(self._o_mesh._nnode_elem):
                        for idime in range(self._ndime):
                            for jdime in range(self._ndime):
                                gp_dshape_glo[inode,idime] +=      \
                                      gp_inv_jacobian[idime,jdime] \
                                    * gp_dshape[inode,jdime]       \
                    #
                    # Interpolate pressure and velocity gradient to gauss point
                    #
                    for inode in range(self._o_mesh._nnode_elem):
                        gp_pressure += elpressure[inode] * gp_shape[inode]
                        for idime in range(self._ndime):
                            for jdime in range(self._ndime):
                                gra_velocity[idime,jdime] += gp_dshape_glo[inode,jdime] \
                                                          * elvelocity[idime,inode]
                    #
                    # Get normal to surface at gauss point 
                    #
                    coords = self._o_mesh._l_node_coords[l_inodes_surf,:]
                    normal = self.calculate_normal_to_element(self._ndime-1, coords)
                    #
                    # Compute stresses
                    #
                    for idime in range(self._ndime):
                        #
                        # Add pressure stresses 
                        #
                        gp_stress[idime] -= gp_pressure * normal[idime]
                        for jdime in range(self._ndime):
                            #
                            # Add viscous stresses 
                            #
                            gp_stress[idime] += self._d_params['rho']                                   \
                                              * self._d_params['nu']                                    \
                                              * (gra_velocity[jdime,idime] + gra_velocity[idime,jdime]) \
                                              * normal[jdime]
                        #
                        # Add gauss point contribution to total stress
                        #
                        stress[iboun,idime] += gp_stress[idime] \
                                             * self._o_mesh._gp_volume_surf[ielem_surf,igaus_surf]
        return stress
