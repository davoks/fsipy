import numpy as np
import time as time
import inspect,dis
from   mpi4py import MPI


def timeit(method):
    """ Timer decorator. """

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            if MPI.COMM_WORLD.Get_rank() == 0:            
                print('\nElapsed time for '+method.__name__+': '+str((te - ts)*1000)+' ms')
        return result

    return timed


def expecting():
    """ Return how many values the caller is expecting. """
    f = inspect.currentframe()
    f = f.f_back.f_back
    c = f.f_code
    i = f.f_lasti
    bytecode = c.co_code
    instruction = bytecode[i+3]
    if instruction == dis.opmap['UNPACK_SEQUENCE']:
        howmany = bytecode[i+4]
        return howmany
    elif instruction == dis.opmap['POP_TOP']:
        return 0
    return 1



def intersection(list1, list2):
    """ Finds intersection between 2 lists. """

    list1 = list(list1)
    list2 = list(list2)

    l_id1 = []
    l_id2 = []
    values1 = []
    values2 = []

    for id1 in range(len(list1)):
        value1 = list1[id1]
        if value1 in list2:
            id2 = list2.index(value1)

            l_id1.append(id1)
            I_id1.append(id2)
            values1 = values1.append(value1)
            values2 = values2.append(list2[id2])

    return l_id1, l_id2, values1, values2



def intersection_exists(list1, list2):
    """ Returns True if intersection between 2 lists is non-empty,
        otherwise, returns False. """

    for id1 in range(len(list1)):
        value1 = list1[id1]
        if value1 in list2:
            return True

    return False



def search(array, values, algorithm=2):

    if algorithm == 0:
        idx_sort  = array.argsort()
        idx_left  = np.searchsorted(array[idx_sort], values, side='left')
        idx_right = np.searchsorted(array[idx_sort], values, side='right')
        Nidx      = int(np.sum(idx_right - idx_left))
        idx       = np.zeros(Nidx, int)
        nidx, ii  = 0, 0
        for i in range(nidx):
            ii  += nidx
            nidx = idx_right[i] - idx_left[i]
            idx[ii:ii+nidx] = np.arange(idx_left[i], idx_right[i])
        idx = idx_sort[idx]

    elif algorithm == 1:
        idx = [key for key, val in enumerate(array) if val in set(values)]

    elif algorithm == 2:
        idx = [idx for val in values for idx in np.where(array == val)[0]]   

    return idx


def calculate_analytic_field(coords, function, parameters):

    if function == 'gauss':
        field = gauss_field( coords, parameters )
    elif function == 'sine':
        field = sine_field( coords, parameters )
    elif function == 'polynomial':
        field = polynomial_field( coords, polynomial )
    else:
        print('Error! Function type not recognized.')
        field = None

    return field


def gauss_field(coords, parameters):

    nnode = coords.shape[0]
    ndime = coords.shape[1]

    if 'amplitude' in parameters:
        amp = parameters['amplitude']
    else:
        amp = 1.0 

    field = amp * np.ones(nnode, float)
    for idime in range(ndime):
        mu     = parameters['mu'][idime]
        sigma  = parameters['sigma'][idime]
        field *= 1.0/(sigma*np.sqrt(2.0*np.pi)) * np.exp( -(coords[:,idime]-mu)**2 / (2*sigma**2) )

    return field


def sine_field(coords, parameters):
    print('Not implemented yet!')
    exit()


def polynomial_field(coords, parameters):
    print('Not implemented yet!')
    exit()
