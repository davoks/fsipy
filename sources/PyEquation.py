"""
    FsiPy Python Equation Kernel
    ============================
"""

# External libraries
import os
import sys
import time
import copy
import vtk 
import math
import decimal
import numpy                as np
import numpy.linalg         as la
import scipy                as sp
import scipy.sparse         as ssp
import scipy.sparse.linalg  as ssla
import time                 as ti
import pickle               as pkl
from   abc                  import ABC, abstractmethod
from   mpi4py               import MPI

# Internal libraries
import ddmpy
import aux                  as aux
import pprocess             as pp
import FortranFluid         as FortranFluid
import FortranSolid         as FortranSolid
import FortranSolidImplicit as FortranSolidImplicit
from   PyParameters         import *
from   PyTiming             import *

# Settings
np.seterr(divide='ignore', invalid='ignore')


# ============================================================================ #
#                             Equation Class                                   #
# ============================================================================ #
class Equation(Timing):

    # Initialization
    def __init__(self,
                 equation_name,
                 variable_name,
                 variable_dims,
                 ndofs,
                 source,
                 flag_dynamic_lhs,
                 flag_for_lumping,
                 d_params,
                 o_mesh,
                 d_dofs_in_bconds,
                 d_parallel):

        # Attributes
        self._name              = equation_name
        self._variable_name     = variable_name
        self._variable_dims     = variable_dims
        self.source             = source
        self._flag_lump_mass    = d_params['flag_lump_mass']
        self._flag_lump_mass    = flag_for_lumping
        self._flag_dynamic_lhs  = flag_dynamic_lhs
        self._flag_assemble_lhs = True 
        self._module_name       = d_params['module_name']
        self._ndime             = d_params['ndime']
        self._ndofs_total       = d_params['ndofs']
        self._npast             = d_params['npast']
        self._ndofs             = ndofs
        self._nnode             = o_mesh._nnode
        self._nrows             = o_mesh._nelem * o_mesh._nnode_elem**2 * ndofs**2
        self._lhs               = []
        self._lhs_lump          = []
        self._lhs0              = []
        self._idlhs             = []
        self._rhs               = np.zeros((ndofs*self._nnode),float)
        self._force             = np.zeros((ndofs*self._nnode),float)
        self._reaction          = np.zeros((ndofs,self._nnode),float)

        self._d_parallel        = d_parallel

    @Timing()
    def assemble_algebraic_system(self, a_variables, a_sources, a_levelset, tcount, is_iterative=False):
        """ Assembles RHS and optionally LHS for Equation. """
        
        # Update flag for static or dynamic LHS assembly
        self.update_lhs_assembly_flag(tcount)

        # Calculate RHS (and optionally LHS) in Fortran Module
        self._lhs_tmp   = []
        self._idlhs_tmp = []
        str_expr = 'self._lhs_tmp, self._idlhs_tmp, self._rhs, self._lhs_lump = '  \
                 + self._module_name+'.'+self._module_name.lower()                 \
                 + '.assemble_algebraic_system_from_python('                       \
                 + 'self._name, '                                                  \
                 + 'a_variables, '                                                 \
                 + 'a_sources, '                                                   \
                 + 'a_levelset, '                                                  \
                 + 'self._flag_assemble_lhs, '                                     \
                 + 'tcount, '                                                      \
                 + 'self._ndofs, '                                                 \
                 + 'self._nrows, '                                                 \
                 + 'self._nnode, '                                                 \
                 + 'self._ndofs_total, '                                           \
                 + 'self._npast'                                                   \
                 + ')'
        exec(str_expr)

        # Save LHS to SSP sparse format
        if self._flag_assemble_lhs:
            self._lhs   = self._lhs_tmp.copy()
            self._idlhs = self._idlhs_tmp.copy()
            if np.isnan(self._lhs_tmp).any(): print('Error! Found NaNs in LHS_tmp')
            self.make_sparse()
            # Copy original LHS before BC imposition
            self._lhs0 = self._lhs.copy()

        # Copy original RHS before BC imposition
        self._rhs0 = self._rhs.copy()

        # Delete temporary arrays
        del self._lhs_tmp
        del self._idlhs_tmp



    def update_lhs_assembly_flag(self, tcount):
        """ Update flag that indicates if LHS must be assembled or not. """

        if (tcount == 2) and (not self._flag_dynamic_lhs):
            self._flag_assemble_lhs = False



    def make_sparse(self):
        """ Build sparse LHS matrix. """

        if np.isnan(self._lhs).any():
            print('Error! Found NaNs in LHS.')
            exit()
        nrows     = self._ndofs * self._nnode
        self._lhs = ssp.csr_matrix((self._lhs,                            \
                                   (self._idlhs[:,0], self._idlhs[:,1])), \
                                   shape=(nrows, nrows))
        self._idlhs = np.unique(self._idlhs, axis=0)



    def adapt_sharing_dofs(self,ndofs):
        """ Change nodal indices sharing by I with other subdomains in order to adapt them to the number of dofs of an equation.
        """

        # Nodes sharing by I with other subdomains
        d_idofs_interface_I = self._d_parallel['nodal_interface_exchange'].copy()
        
        # Loop over neighbor subdomains of I        
        for key in d_idofs_interface_I:
            new_len                            = len(d_idofs_interface_I[key])*ndofs
            l_indices_new                      = np.ones(new_len,int)
            l_indices_old                      = np.array(d_idofs_interface_I[key],int)

            # Change indices in order to adapt them to the number of dofs                        
            for i in range(ndofs):
                l_indices_new[i:new_len:ndofs] = l_indices_old*ndofs + i
            d_idofs_interface_I[key]           = l_indices_new.tolist()
            
        return  d_idofs_interface_I


        
    def build_distributed_matrix(self):        
        """ Define the matrix in terms of a distributed one in order to solve
            it with the parallel hybrid solver.
        """
        # Determine the dofs in the interface between the subdomains
        d_idofs_interface_I = self.adapt_sharing_dofs(self._ndofs)
        
        # Define the matrix in terms of the DOMAIN DECOMPOSTION class        
        domain_decomposition = ddmpy.DomainDecomposition(self._nnode*self._ndofs, d_idofs_interface_I, MPI.COMM_WORLD)
        self._lhs_dd         = ddmpy.DistMatrix(self._lhs, domain_decomposition)        

        
    @Timing()
    def solve(self,is_parallel):
        """ Solve algebraic system. """

        # ------------------------------------------------------------------- #
        #             If problem is parallel => use ddmpy solver              #
        # ------------------------------------------------------------------- #
        if is_parallel:

            # Solve algebraic system
            if self._flag_lump_mass:
                pass
            else:
                if self._flag_assemble_lhs:                                    
                    self.build_distributed_matrix()
                            
                # Define the vector in terms of the DOMAIN DECOMPOSTION class        
                self._rhs_dd = ddmpy.DistVector(self._rhs, self._lhs_dd.dd, assemble=True)
            
                # Define the hybrid solver characteristics for the DOMAIN DECOMPOSTION class
                solver = ddmpy.DistSchurSolver(interface_solver = ddmpy.GCR( M=ddmpy.AdditiveSchwarz,
                                                                         maxiter=1000, tol=1e-10))
            
                # Add the partitioned matrix in the solver                
                solver.setup(self._lhs_dd)

                # Solve the problem
                results_dd = solver.dot(self._rhs_dd)
                variable   = results_dd.local.reshape(-1)

        # ------------------------------------------------------------------- #
        #             If problem is serial => use scipy                       #
        # ------------------------------------------------------------------- #
        else:

            # Solve algebraic system
            if self._flag_lump_mass:
                variable = np.divide(self._rhs, self._lhs)
            else:
                variable = ssla.spsolve(self._lhs, self._rhs)

        return variable.reshape((self._ndofs,-1),order='F')



    def iterate(self, variable):
        """ Do modified richardson iteration. """

        variable = variable.reshape((-1),order='F')

        # Optional lhs matrix lumping (by row)
        if self._flag_assemble_lhs:
            if self._flag_lump_mass:
                #nrows = self._ndofs*self._nnode
                #lhs_lumped = np.zeros(nrows, float)
                #for irow in range(nrows):
                #    lhs_lumped[irow] = np.sum(self._lhs[irow,:])
                #self._lhs = lhs_lumped
                self._lhs = self._lhs_lump

        if self._flag_lump_mass:
            variable = np.divide(self._rhs, self._lhs)
        else:
            variable = ssla.spsolve(self._lhs, self._rhs)
       
        return variable.reshape((self._ndofs,-1),order='F')



    def impose_boundary_conditions_on_algebraic_system(self,
                                                       d_bconds,
                                                       o_mesh,
                                                       d_dofs_in_bconds,
                                                       d_variables,
                                                       l_variable_names,
                                                       l_variables_types,
                                                       is_iterative,
                                                       is_parallel,
                                                       d_parallel):
        """ Impose boundary conditions in LHS and RHS of Equation. 
            
            IMPORTANT:
            Here, we impose always condition on the primary variable of an equation defined
            in a particular problem. Primary variable will be the variable considered in 
            the system of equations.
    
        """

        for btype in d_bconds[self._name]:
            for iphysical, boundary_condition in d_bconds[self._name]['nodes'].items():
                               
                # Get nodes at physical entity
                l_inodes = boundary_condition['l_inodes']

                for idof in range(self._ndofs):
                    l_irows  = [self._ndofs*inode + idof for inode in l_inodes]

                    ## Dirichlet BCs
                    ## -------------
                    if boundary_condition['types'][idof] == 1:

                        # Impose Dirichlet BC on RHS
                        # --------------------------
                        # Use value of variable to set BC
                        #if flag_derive_bcond:
                        #    self._rhs[l_irows] = d_variables[self._variable_name][idof,l_inodes,iter_k]
                        # Space-time function
                        if boundary_condition['function_type'] == 'space-time_function':
                            self._rhs[l_irows] = boundary_condition['values'][:,idof]
                        # Time function
                        elif boundary_condition['function_type'] == 'time_function':
                            self._rhs[l_irows] = boundary_condition['values'][idof]
                        # BC from coupling 
                        elif boundary_condition['function_type'] == 'from_coupling':
                            self._rhs[l_irows] = boundary_condition['values'][:,idof]
                        # BCs homogeneous in space
                        else:
                            self._rhs[l_irows] = boundary_condition['values'][idof]

                        # In parallel we have to divide the BC for the number of nodes shared with other subdomains
                        if is_parallel:
                            for inode in l_inodes:
                                irow = self._ndofs*inode + idof
                                self._rhs[irow] =  self._rhs[irow]/d_parallel['shared_nodes_number'][inode]

                        # Impose Dirichlet BC on LHS
                        # --------------------------
                        if self._flag_assemble_lhs:
                            # Eliminate all Dirichlet rows
                            l_ids_rows = [idx for irow in l_irows for idx in np.where(self._idlhs[:,0]==irow)[0]]
                            l_irows4rows_ssp = self._idlhs[l_ids_rows,0]
                            l_icols4rows_ssp = self._idlhs[l_ids_rows,1]
                            self._lhs[l_irows4rows_ssp, l_icols4rows_ssp] = 0.0
                            # Set all Dirichlet diagonals to 1
                            self._lhs[l_irows, l_irows]                   = 1.0 # / nshare
                            
                            # Impose bpundary condition also in the lumping lhs matrix (that it could be used)
                            self._lhs_lump[l_irows]                       = 1.0 # / nshare

                            
                    ## Force BCs
                    ## ---------
                    #elif bcond['types'][idof] == 2:
                    #
                    #    # Sum nodal force on RHS
                    #    # ----------------------
                    #    # Use value of variable to set BC
                    #    # Space-time function
                    #    if bcond['function_type'] == 'space-time_function':
                    #        self._rhs[l_irows] += bcond['values'][:,idof]
                    #    # Time function
                    #    elif bcond['function_type'] == 'time_function':
                    #        self._rhs[l_irows] += bcond['values'][idof]
                    #    # BCs homogeneous in space
                    #    else:
                    #        self._rhs[l_irows] += bcond['values'][idof]
                    
        ### Set boundary conditions on elements
        ### -----------------------------------
        #if d_dofs_in_bconds['elements'] == self._variable_name:
        #
        #    ## Strong pressure BCs
        #    ## -------------------
        #    # -------------------------------------------------------------------------- #
        #    # *TODO* : Correct implementation of Neumann BCs for Navier-Stokes F-S eqns. #
        #    # -------------------------------------------------------------------------- #
        #    if self._variable_name == 'pressure':
        #
        #        # Loop over physical entities
        #        for iphysical, bcond in d_bconds['elements'].items():
        #    
        #            # Get nodes at physical entity
        #            l_irows = bcond['l_inodes']
        #    
        #            if bcond['types'] == 2:
        #    
        #                # Impose pressure BC strongly on RHS
        #                # ----------------------------------
        #                # Space-time function
        #                if bcond['function_type'] == 'space-time_function':
        #                    self._rhs[l_irows] = bcond['values'][0]
        #                # Time function
        #                elif bcond['function_type'] == 'time_function':
        #                    self._rhs[l_irows] = bcond['values'][0]
        #                # BCs homogeneous in space
        #                else:
        #                    self._rhs[l_irows] = bcond['values'][0]
        #
        #    
        #                # Impose pressure BC strongly on LHS
        #                # ----------------------------------
        #                if self._flag_assemble_lhs:
        #                    l_ids_in_rows = [idx for irow in l_irows for idx in np.where(self._idlhs[:,0]==irow)[0]] 
        #                    l_irows_in_ssp = [self._idlhs[idx,0] for idx in l_ids_in_rows]
        #                    l_icols_in_ssp = [self._idlhs[idx,1] for idx in l_ids_in_rows]
        #                    self._lhs[l_irows_in_ssp, l_icols_in_ssp] = 0.0
        #                    self._lhs[l_irows,        l_irows]        = 1.0 # / nshare
        #
        #    ## Usual Neumann BCs
        #    ## -----------------
        #    else:
        #
        #        # Loop over physical entities
        #        traction = np.zeros((self._ndofs,o_mesh._nelem_surf),float)
        #        for iphysical, bcond in d_bconds['elements'].items():
        #    
        #            # Get elements and nodes at physical entity
        #            l_ielems = bcond['l_ielements']
        #
        #            # Get values of Neumann BC
        #            for idof in range(self._ndofs):
        #                if bcond['types'][idof] == 2:
        #                    # Space-time function
        #                    if bcond['function_type'] == 'space-time_function':
        #                        traction[idof,l_ielems] = bcond['values'][:,idof]
        #                    # Time function
        #                    elif bcond['function_type'] == 'time_function':
        #                        traction[idof,l_ielems] = bcond['values'][idof]
        #                    # BCs homogeneous in space
        #                    else:
        #                        traction[idof,l_ielems] = bcond['values'][idof]
        #
        #        # Assemble neumann BC on RHS
        #        # ----------------------------------------------- #
        #        # *TODO* : Port this piece to the Fortran Kernel. #
        #        # ----------------------------------------------- #
        #        # loop over degrees of freedom
        #        for idof in range(self._ndofs):
        #            if bcond['types'][idof] == 2:
        #                # loop over surface elements
        #                for ielem in range(o_mesh._nelem_surf):
        #                    l_inodes_elem = o_mesh._l_elems_surf[ielem,:]
        #                    # loop over gauss points
        #                    for igaus in range(o_mesh._ngaus_surf):
        #                        # loop over nodes of surface element
        #                        for inode_loc, inode_surf in enumerate(l_inodes_elem):
        #                            # get volume node index
        #                            inode_vol = inode_surf #get_node_id_volume2surf(inode_surf)
        #                            # get row index in RHS
        #                            irow = int(inode_vol * self._ndofs + idof)
        #                            # integrate and sum traction to RHS
        #                            self._rhs[irow] +=                               \
        #                                  o_mesh._gp_shape_surf[igaus,inode_loc]     \
        #                                * o_mesh._gauss_weights_surf[igaus]          \
        #                                * o_mesh._gp_det_jacobians_surf[ielem,igaus] \
        #                                * traction[idof,ielem]
        #
        #
        #### Set BCs to 0 on boundary nodes by default
        #### -----------------------------------------
        #if d_dofs_in_bconds['zero'] == self._variable_name:
        #
        #    # Loop over all physical entities
        #    for dim, iphysical in o_mesh.getPhysicalGroups():
        #        if dim < self._ndime:
        #
        #            # Get nodes at physical entity
        #            l_inodes,_ = o_mesh.getNodesForPhysicalGroup(dim=dim, tag=iphysical)
        #            l_inodes = l_inodes.astype(int) - 1
        #
        #            for idof in range(self._ndofs):
        #                l_irows = [self._ndofs*inode + idof for inode in l_inodes]
        #
        #                # Impose BC on RHS
        #                # ----------------
        #                self._rhs[l_irows] = 0.0 
        #
        #                # Impose BC on LHS
        #                # ----------------
        #                if self._flag_assemble_lhs:
        #                    l_ids_in_rows = [idx for irow in l_irows for idx in np.where(self._idlhs[:,0]==irow)[0]] 
        #                    l_irows_in_ssp = [self._idlhs[idx,0] for idx in l_ids_in_rows]
        #                    l_icols_in_ssp = [self._idlhs[idx,1] for idx in l_ids_in_rows]
        #                    self._lhs[l_irows_in_ssp, l_icols_in_ssp] = 0.0
        #                    self._lhs[l_irows,        l_irows]        = 1.0 # / nshare
        
        
        # Lump LHS matrix if activated
        # ----------------------------
        self.lump_lhs_matrix()



    def impose_zero_boundary_conditions_on_algebraic_system(self,
                                                            d_bconds,
                                                            o_mesh,
                                                            d_dofs_in_bconds,
                                                            d_variables,
                                                            l_variable_names,
                                                            l_variables_types,
                                                            is_iterative,
                                                            is_parallel,
                                                            d_parallel):
        """ Impose boundary conditions in LHS and RHS of Equation. """
        
        for btype in d_bconds[self._name]:
            for iphysical, boundary_condition in d_bconds[self._name]['nodes'].items():    
                # Get nodes at physical entity
                l_inodes = boundary_condition['l_inodes']
                
                for idof in range(self._ndofs):
                    l_irows  = [self._ndofs*inode + idof for inode in l_inodes]
    
                    ## Dirichlet BCs
                    ## -------------
                    if boundary_condition['types'][idof] == 1:
    
                        # Impose Dirichlet BC on RHS
                        # --------------------------
                        self._rhs[l_irows] = 0.0 
    
                        # In parallel we have to divide the BC for the number of nodes shared with other subdomains
                        if is_parallel:
                            for inode in l_inodes:
                                irow = self._ndofs*inode + idof
                                self._rhs[irow] =  self._rhs[irow]/d_parallel['shared_nodes_number'][inode]
    
                        # Impose Dirichlet BC on LHS
                        # --------------------------
                        if self._flag_assemble_lhs:
                            for ipoint in l_irows:
                                # Eliminate all Dirichlet rows
                                l_ids_rows = [idx for irow in l_irows for idx in np.where(self._idlhs[:,0]==irow)[0]]
                                l_irows4rows_ssp = [self._idlhs[idx,0] for idx in l_ids_rows]
                                l_icols4rows_ssp = [self._idlhs[idx,1] for idx in l_ids_rows]
                                self._lhs[l_irows4rows_ssp, l_icols4rows_ssp] = 0.0
                                # Eliminate all Dirichlet columns
                                l_ids_cols = [idx for icol in l_irows for idx in np.where(self._idlhs[:,1]==icol)[0]]
                                l_irows4cols_ssp = [self._idlhs[idx,0] for idx in l_ids_cols]
                                l_icols4cols_ssp = [self._idlhs[idx,1] for idx in l_ids_cols]
                                self._lhs[l_irows4cols_ssp, l_icols4cols_ssp] = 0.0
                                # Set all Dirichlet diagonals to 1
                                self._lhs[l_irows,        l_irows]        = 1.0 # / nshare

                                # Impose bpundary condition also in the lumping lhs matrix (that it could be used)
                                self._lhs_lump[l_irows]                  = 1.0 # / nshare
                                
        # Lump LHS matrix if activated
        # ----------------------------
        self.lump_lhs_matrix()

    def lump_lhs_matrix(self):

        # Optional lhs matrix lumping (by row)
        if self._flag_assemble_lhs:
            if self._flag_lump_mass:
                #nrows = self._ndofs*self._nnode
                #lhs_lumped = np.zeros(nrows, float)
                #for irow in range(nrows):
                #    lhs_lumped[irow] = np.sum(self._lhs[irow,:])
                #self._lhs = lhs_lumped
                self._lhs = self._lhs_lump



    def impose_boundary_conditions_on_variables(self,
                                                d_bconds,
                                                o_mesh,
                                                d_variables,
                                                d_sources,
                                                d_dofs_in_bconds,
                                                is_iterative):
        """ Imposes boundary conditions directly on variables. 

            IMPORTANT: 
            Here, we impose directly the Dirichlet boundary values on a variable.
            This variable defined for an equation in "d_dofs_in_bconds" could be a secondary variable;
            that is, a variable that it is not defined in the system of equations (see PySolid for an example.)
                       
        """

        # Variable name where we will impose the Dirichlet boundary values
        variable_name = d_dofs_in_bconds[self._name]
        variable      = d_variables[self._variable_name]

        for iphysical, boundary_condition in d_bconds[self._name]['nodes'].items():
            # Get nodes at physical entity
            l_inodes = boundary_condition['l_inodes']

            for idof in range(self._ndofs):
                if boundary_condition['types'][idof] == 1:

                    # Impose BC on RHS
                    # ----------------
                    # Space-time function
                    if boundary_condition['function_type'] == 'space-time_function':
                        variable[idof,l_inodes,iter_k] = boundary_condition['values'][:,idof]
                    # Time function
                    elif boundary_condition['function_type'] == 'time_function':
                        variable[idof,l_inodes,iter_k] = boundary_condition['values'][idof]
                    # From coupling 
                    if boundary_condition['function_type'] == 'from_coupling':
                        if 'values' in boundary_condition:
                            # Impose value on variable involved in coupling
                            cou_var_name = boundary_condition['variable_name']
                            d_variables[cou_var_name][idof,l_inodes,iter_k] = boundary_condition['values'][:,idof]
                    # BCs homogeneous in space
                    else:
                        variable[idof,l_inodes,iter_k] = boundary_condition['values'][idof]

        return d_variables, d_sources



    def calculate_reaction(self, d_variables, is_iterative):
        """ Computes algebraic reaction force as:

                r = b - A * x

            where b : RHS vector
                  A : LHS matrix before Dirichlet BC imposition
                  x : solution

            Outputs:
            --------
                self._reaction : np-array with dims (ndofs*nnode,)
                    algebraic reaction force
        """

        # Compute algebraic reaction force
        variable = d_variables[self._variable_name][:,:,iter_k].reshape((-1),order='F')
        reaction = self._rhs0 - self._lhs0.dot( variable )
        reaction = reaction.reshape((self._ndofs,-1),order='F')

        # If function is called without output arguments => save as Equation attribute
        noutputs = aux.expecting()
        if (noutputs == 0):
            self._reaction = reaction
        # Else => return reaction force
        else:
            return reaction



    def calculate_force_from_stress(self, a_variables, a_sources, is_iterative=False):
        """ Computes force as divergence of stress tensor.

                f = d/dxi sigma_ij

            Outputs:
            --------
                self._force : np-array with dims (ndofs*nnode,)
                    force from stress tensor
        """

        # Compute force
        str_expr = 'self._force = '  \
                 + self._module_name+'.'+self._module_name.lower() \
                 + '.assemble_force_from_python('                  \
                 + 'self._name, '                                  \
                 + 'a_variables, '                                 \
                 + 'a_sources, '                                   \
                 + 'self._ndofs, '                                 \
                 + 'self._nrows, '                                 \
                 + 'self._nnode, '                                 \
                 + 'self._ndofs_total, '                           \
                 + 'self._npast'                                   \
                 + ')'
        exec(str_expr)
        self._force = self._force.reshape((self._ndofs,-1),order='F')

        return self._force
# ============================================================================ #
