from __future__ import print_function
from collections import OrderedDict
import traceback
import time
import numpy as np
import scipy.sparse as ssp
import scipy.linalg as la
import scipy.sparse.linalg as sla
from mpi4py import MPI

#try:
#    import pypastix
#except ImportError:
#    traceback.print_exc()
#
#try:
#    import pymumps
#except ImportError:
#    traceback.print_exc()
#
#try:
#    import pymaphys
#except ImportError:
#    traceback.print_exc()

def neighborSendRecv(messages, comm=MPI.COMM_WORLD, n_recv=None,
                     is_ndarray=None, dtype=None, debug=False):
    if n_recv is None:
        n_recv = len(messages)
    if is_ndarray is None:
        is_ndarray = all(isinstance(v, np.ndarray)
                         for v in messages.values())
    if is_ndarray and dtype is None:
        assert(len(messages) > 0)
        dtype = list(messages.values()).pop().dtype
        assert(all(v.dtype == dtype
                   for v in messages.values()))
    tag = getattr(neighborSendRecv, "tag", 0)
    neighborSendRecv.tag = tag + 1
    # Send
    requests = []
    for dest, message in messages.items():
        if debug:
            print("{}.-> {}: {} / {} {}".format(
                comm.Get_rank(), dest, message,
                tag, is_ndarray))
        if is_ndarray:
            req = comm.Isend(message, dest, tag)
        else:
            req = comm.isend(message, dest, tag)
        requests.append(req)
    # Probe and Receive
    status = MPI.Status()
    while n_recv > 0:
        comm.Probe(MPI.ANY_SOURCE, tag, status)
        source = status.Get_source()
        if is_ndarray:
            size = status.Get_count(
                MPI._typedict[np.dtype(dtype).char])
            message = np.empty(size, dtype)
            comm.Recv(message, source, tag)
        else:
            message = comm.recv(None, source, tag)
        if debug:
            print("{} ->.{}: {} / {}{}".format(
                source, comm.Get_rank(), message,
                tag, is_ndarray))
        yield source, message
        n_recv -= 1
    # Wait for Send to terminate
    for req in requests:
        req.wait()

class TimeIt:

    debug = False
    max_events = None
    events = []
    stack = []
    t0 = time.time()

    def __init__(self, name=""):
        self.name = name

    def __enter__(self):
        self.begin = time.time() - self.t0
        self.end = np.nan
        self.duration = np.nan
        self.level = len(self.stack)
        self.events.append(self)
        self.stack.append(len(self.events)-1)
        if TimeIt.debug:
            print(self.__str__(which="current"))
        return self

    def __exit__(self, type, value, traceback):
        self.end = time.time() - self.t0
        self.duration = self.end - self.begin
        if TimeIt.debug:
            print(self.__str__(which="current"))
        if self.stack:
            self.stack.pop()
        return False

    def __call__(self, f):
        def timed_f(*args, **kwargs):
            # We create a default name
            name = self.name
            if name=="":
                name = type(args[0]).__name__ + " " + f.__name__
            # We time and run the function
            # We create a new TimeIt instance because self is created
            # at function declaration and is the same object for all
            # executions of the function
            with TimeIt(name) as t:
                res = f(*args, **kwargs)
            # Store the duration in the object
            if len(args)>0 and isinstance(args[0], LinearOperator):
                solver = args[0]
                key = "t_" +  name.replace(" ", "_")
                if key in solver.parameters:
                    solver.parameters[key] += t.duration
                else:
                    solver.parameters[key] = t.duration
            return res
        return timed_f

    def __str__(self, which="all"):
        if which=="current":
            s = "{:50s} | {:2d} | {:12.7f} | {:12.7f} |\
            {:12.7f}".format( "!  "*self.level + self.name,
                              self.level, self.begin,
                              self.duration, self.end)
        elif which=="all":
            s = "\n".join(
                [t.__str__(which="current")
                 for t in self.events[:TimeIt.max_events]])
        else: # "stack"
            s = "\n".join(
                [self.events[i].__str__(which="current")
                 for i in self.stack])
        return s

    @classmethod
    def reset(cls, comm=MPI.COMM_WORLD):
        cls.events = []
        cls.stack = []
        if comm is not None:
            comm.barrier()
        cls.t0 = time.time()

class DomainDecomposition(object):

    def __init__(self, ni, neighbors, comm=MPI.COMM_WORLD):
        self.ni = ni
        self.neighbors = neighbors
        self.interface = np.unique(np.hstack(neighbors.values()))
        self.nG = len(self.interface)
        self.nI = ni - self.nG
        self.comm = comm
        self.n_subdomains = comm.Get_size()
        self.rank = comm.Get_rank()
        # Boolean partition of unity
        # for scalar products
        self.D = np.ones((ni, 1), dtype=np.int)
        for n in neighbors:
            if n < self.rank:
                self.D[neighbors[n], 0] = 0
        # Compute the global ordering
        # Number of unknowns the subdomain is responsible for
        ind_responsible = np.nonzero(self.D)[0]
        n_responsible = len(ind_responsible)
        n_per_subdomain = np.empty(self.n_subdomains, dtype=int)
        comm.Allgather(
            np.array(n_responsible),
            n_per_subdomain)
        # Total number of unknowns
        self.n = n_per_subdomain.sum()
        # Number of unknowns of smaller rank
        offset = n_per_subdomain[:self.rank].sum()
        # global numbering of the subdomain's unknowns
        self.global_indices = np.zeros((self.ni), dtype=int)
        self.global_indices[ind_responsible] = range(
            offset, offset+n_responsible)
        # Send global numbering of shared nodes to neighbors
        send = {n: self.global_indices[neighbors[n]]
                for n in neighbors if self.rank<n}
        n_recv = len([n for n in neighbors if n<self.rank])
        for n, recv in neighborSendRecv(
                send, n_recv=n_recv, is_ndarray=True, dtype=int):
            self.global_indices[neighbors[n]] += recv.ravel()

    def assemble(self, v, dim=1, debug=False):
        comm = self.comm
        if dim==1:   # vector assembly
            indices = self.neighbors
        elif dim==2: # matrix assembly
            indices = {n: np.ix_(i,i) if len(i)>1 else (i,i)
                       for n, i in self.neighbors.items()}
        else:
            raise ValueError("dim should be 1 or 2")
        v_ = v.copy()
        send = {j: v[indices[j]] for j in indices}
        for j, recv in neighborSendRecv(send, debug=debug):
            if recv.shape != send[j].shape:
                recv = recv.reshape(send[j].shape)
            v_[indices[j]] += recv
        return v_

    def interface_dd(self):
        """return the DomainDecomposition of the interface matrix"""
        neighbors = {n: np.searchsorted(self.interface, i).tolist()
                     for n, i in self.neighbors.items()}
        return DomainDecomposition(len(self.interface), neighbors,
                                   self.comm)

    @staticmethod
    def test():
        # see Figure $\ref{fig:py:example}$
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        assert(comm.Get_size() == 3)
        if rank==0:   # $\Omega_1$
            neighbors = {1: [2, 3], # interface with $\Omega_2$
                         2: [0, 3]} # interface with $\Omega_3$
            ni = 4                  # number of local unknowns
        elif rank==1: # $\Omega_2$
            neighbors = {0: [1, 0], # interface with $\Omega_1$
                         2: [0, 3]} # interface with $\Omega_3$
            ni = 4                  # number of local unknowns
        elif rank==2: # $\Omega_3$
            neighbors = {0: [2, 3], # interface with $\Omega_1$
                         1: [3, 0]} # interface with $\Omega_2$
            ni = 4                  # number of local unknowns
        dd = DomainDecomposition(ni, neighbors)
        return dd

class DistVector(object):

    def __init__(self, bi, dd, compatible=True,
                 assemble=False):
        if len(bi.shape) != 2:
            bi = bi.reshape((dd.ni, -1))
        if assemble:
            self.local = dd.assemble(bi)
        else:
            self.local = bi
        self.dd = dd
        self.shape = dd.n, bi.shape[1]
        self.ndim = len(self.shape)
        self.transposed = False
        self.compatible = compatible

    def __len__(self):
        return self.shape[0]

    def copy(self):
        return DistVector(self.local.copy(), self.dd,
                          self.compatible)

    @property
    def T(self):
        other = DistVector(self.local, self.dd, self.compatible)
        other.transposed = not self.transposed
        other.shape = self.shape[::-1]
        return other

    @TimeIt()
    def __add__(self, other): # self + other
        with TimeIt("Local __add__"):
            return DistVector(self.local + other.local,
                              self.dd, self.compatible)

    @TimeIt()
    def __sub__(self, other): # self - other
        with TimeIt("Local __sub__"):
            return DistVector(self.local - other.local,
                              self.dd, self.compatible)

    @TimeIt()
    def __rmul__(self, scalar): # scalar * self
        with TimeIt("Local __rmul__"):
            return DistVector(scalar * self.local,
                              self.dd, self.compatible)

    @TimeIt()
    def __mul__(self, scalar): # self * scalar
        with TimeIt("Local __mul__"):
            return DistVector(self.local * scalar,
                              self.dd, self.compatible)

    @TimeIt()
    def __div__(self, scalar): # self / scalar
        with TimeIt("Local __div__"):
            return DistVector(self.local / scalar,
                              self.dd, self.compatible)

    def __truediv__(self, scalar): # self / scalar
        return self.__div__(scalar)

    @TimeIt()
    def dot(self, other):
        dd = self.dd
        if self.transposed: # self.T @ other
            # multiplication along the distributed dimension
            assert(self.dd==other.dd)
            if not isinstance(other, DistVector):
                return NotImplemented
            with TimeIt("Local dot"):
                if self.compatible:
                    # don't count redundant entries twice
                    local_dot = self.local.T.dot(
                        self.dd.D*other.local)
                else:
                    local_dot = self.local.T.dot(other.local)
            with TimeIt("MPI reduce"):
                global_dot = dd.comm.allreduce(local_dot)
            return  global_dot
        else: # self @ other
            # multiplication along the global dimension
            return DistVector(self.local.dot(other), dd,
                              self.compatible)

    def __matmul__(self, other): # self @ other
        return self.dot(other)

    def centralize(self, root=None, loc2glob=None):
        if loc2glob is None:
            loc2glob = self.dd.global_indices
        b_global = np.zeros((self.dd.n, self.shape[1]), self.local.dtype)
        a = self.dd.D * self.local
        b_global[loc2glob] = a
        if root is None:
            self.dd.comm.Allreduce(b_global.copy(), b_global)
        else:
            self.dd.comm.Reduce(b_global.copy(), b_global, root=root)
        return b_global

class DistMatrix(object):

    def __init__(self, Ai, dd):
        self.local = Ai
        self.dd = dd
        self.shape = dd.n, dd.n

    def copy(self):
        return DistMatrix(self.local.copy(), self.dd)

    @TimeIt()
    def dot(self, b, transpose=False):
        assert(isinstance(b, DistVector))
        assert(self.dd==b.dd)
        with TimeIt("Local dot"):
            if transpose:
                Abi = self.local.T.dot(b.local)            # $\Ai^T b_i$
            else:
                Abi = self.local.dot(b.local)              # $\Ai  b_i$
        return DistVector(Abi, self.dd, assemble=True) # $\Ri \A b$

    def __matmul__(self, b):
        return self.dot(b)

    def __rmatmul__(self, b):
        return self.dot(b.T, transpose=True).T

    def centralize(self, root=None, loc2glob=None):
        """return the global matrix on process root.
        If no root is provided, then all get the global matrix
        """
        if loc2glob is None:
            loc2glob = self.dd.global_indices
        A_global_i = ssp.lil_matrix((self.dd.n, self.dd.n))
        A_global_i[np.ix_(loc2glob, loc2glob)] = self.local
        if root is None:
            return self.dd.comm.allreduce(A_global_i)
        else:
            return self.dd.comm.reduce(A_global_i, root=root)

    @staticmethod
    def test():
        dd = DomainDecomposition.test()
        Ki = np.array(
            [[ 3, -1,  0, -1],
             [-1,  4, -1, -1],
             [ 0, -1,  3, -1],
             [-1, -1, -1,  4]], dtype=float)
        K = DistMatrix(Ki, dd)
        return K

class LinearOperator(object):

    defaults = {}

    def __init__(self, A=None, **kwargs):
        """ Constructor of the solver

        Store the keyword arguments as parameters for the solver,
        performs all the analysis steps that are possible without
        having the matrix A
        and performs the setup if the matrix A is provided
        """
        if hasattr(self, "defaults"):
            self.parameters = OrderedDict(
                sorted(self.defaults.items(),
                       key=lambda x:str.lower(x[0])))
            self.parameters.update(kwargs)
        else:
            self.parameters = OrderedDict(kwargs.items())
        self.setup_performed = False
        if A is not None:
            self.setup(A)

    # abstract method
    def setup(self, A, **kwargs):
        """ Setups the solver to work on a particular matrix A

        Store the keyword arguments as parameters for the solver
        and performs all computational steps that are possible
        without having the RHS.
        """
        self.A = A
        self.shape = A.shape
        self.parameters.update(kwargs)
        # some default parameters are solver types instead of
        # instance and a type v has to be replaced by an
        # instance of v
        for k, v in self.parameters.items():
            if type(v)==type:
                instance = v.__new__(v)
                instance.__init__()
                self.parameters[k] = instance
        self.setup_performed = True

    # abstract method
    def solve(self, b):
        """Performs a solve with b as a right-hand side vector.
        A subclass may allow multiple rhs to be specified as a
        matrix.
        """
        pass

    def dot(self, b):
        if self.setup_performed:
            return self.solve(b)
        else:
            raise RuntimeError("The operator was not setup before"
                               " performing the solve")

    def __matmul__(self, b): # self @ b
        return self.dot(b)

    def matvec(self, b): # scipy.sparse.linalg.aslinearoperator()
        return self.dot(b)

    def __str__(self, level=0):
        s = [self.__class__.__name__]
        for key, value in self.parameters.items():
            k = str(key)
            if isinstance(value, LinearOperator):
                v = value.__str__(level+1)
            else:
                v = str(value)
            s.append("  "*(1+level) + k + " : " + v)
        return "\n".join(s)

    def __getattr__(self, item):
        if item != "parameters" and item in self.parameters:
            return self.parameters[item]
        elif item in self.__dict__:
            return self.__dict__[item]
        else:
            raise AttributeError("No {} in {}."
                                 .format(item, self))

class ScipyDirectSolver(LinearOperator):
    """Factorize a matrix using scipy

    Choose an appropriate Scipy solver according to the
    format (dense/sparse) and symmetry of A. Optional
    parameters are:
    - symmetry=True/False. Default: False
    """

    defaults = {"symmetry": False}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        if ssp.issparse(A):
            self.solve_ = sla.factorized(A)
        else:
            if self.symmetry in {2, "SPD"}:
                self.LLt = la.cho_factor(A)
                self.solve_ = lambda b:la.cho_solve(
                    self.LLt, b)
            else:
                self.LU = la.lu_factor(A)
                self.solve_ = lambda b: la.lu_solve(
                    self.LU, b)

    @TimeIt()
    def solve(self, b):
        return self.solve_(b)

class Pinv(LinearOperator):

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A)
        if ssp.issparse(A):
            self.A_pinv = la.pinv(A.A)
        else:
            self.A_pinv = la.pinv(A)

    @TimeIt()
    def solve(self, b):
        return self.A_pinv.dot(b)

class Pastix(LinearOperator):
    """Factorize a matrix using pastix
    """

    defaults = {"verbose": False,
                "symmetry": False,
                "refine": True,
                "threads": 1,
                "check": False,
                "x0": None}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        self.init(A)
        pypastix.task_analyze(self.pastix_data, self.spmA)
        pypastix.task_numfact(self.pastix_data, self.spmA)

    def init(self, A):
        """ Register the options in iparm and dparm, and setup A"""
        iparm, dparm = pypastix.initParam()
        self.iparm, self.dparm = iparm, dparm
        # Verbose
        d = {0: pypastix.verbose.Not,
             1: pypastix.verbose.Yes,
             2: pypastix.verbose.No}
        iparm[pypastix.iparm.verbose] = d[self.verbose]
        # Threads
        if self.threads=="auto":
            iparm[pypastix.iparm.thread_nbr] = -1
        else:
            iparm[pypastix.iparm.thread_nbr] = self.threads
        # Symmetry
        d = {0: pypastix.factotype.LU,
             1: pypastix.factotype.LDLT,
             2: pypastix.factotype.LLT,
             "SPD": pypastix.factotype.LLT}
        self.factotype = d[self.symmetry]
        self.iparm[pypastix.iparm.factorization] = self.factotype
        # init
        self.pastix_data = pypastix.init(iparm, dparm)
        self.spmA = pypastix.spm(A)
        if self.verbose:
            self.spmA.printInfo()

    @TimeIt()
    def solve(self, b):
        x = b.copy()
        pypastix.task_solve(self.pastix_data, x)
        if self.refine:
            pypastix.task_refine(self.pastix_data, b, x)
        if self.check and self.x0 is not None:
            self.spmA.checkAxb(self.x0, b, x)
        return x

    @TimeIt()
    def schur(self, A, interface, **kwargs):
        """Perform a partial factorization and compute
        the Schur matrix S
        """
        LinearOperator.setup(self, A, **kwargs)
        self.init(A)
        self.iparm[pypastix.iparm.schur_solv_mode] = (
                pypastix.solv_mode.Interface)
        schur_list = np.asarray(interface, pypastix.pastix_int)
        self.schur_list = schur_list + self.spmA.findBase()
        pypastix.setSchurUnknownList(self.pastix_data,
                                     self.schur_list)
        pypastix.task_analyze(self.pastix_data, self.spmA)
        pypastix.task_numfact(self.pastix_data, self.spmA)
        nschur = len(schur_list)
        self.nschur = nschur
        self.S = np.zeros((nschur, nschur),
                          order='F', dtype=A.dtype)
        pypastix.getSchur(self.pastix_data, self.S)
        return self.S

    @TimeIt()
    def b2f(self, b):
        """Compute the reduced RHS f from the
        complete RHS b
        """
        x = b.copy()
        pypastix.subtask_applyorder(
            self.pastix_data, pypastix.dir.Forward, x)
        if self.factotype == pypastix.factotype.LLT:
            pypastix.subtask_trsm(
                self.pastix_data, pypastix.side.Left,
                pypastix.uplo.Lower, pypastix.trans.NoTrans,
                pypastix.diag.NonUnit, x)
        else:
            pypastix.subtask_trsm(
                self.pastix_data, pypastix.side.Left,
                pypastix.uplo.Lower, pypastix.trans.NoTrans,
                pypastix.diag.Unit, x)
        if self.factotype == pypastix.factotype.LDLT:
            pypastix.subtask_diag(self.pastix_data, x)
        self.x = x
        f = x[-self.nschur:]
        return f

    @TimeIt()
    def y2x(self, y, b):
        """ Compute the complete solution x
        from the Schur solution y
        """
        x = self.x.copy()
        x[-self.nschur:] = y
        if self.factotype == pypastix.factotype.LDLT:
            pypastix.subtask_trsm(
                self.pastix_data, pypastix.side.Left,
                pypastix.uplo.Lower, pypastix.trans.Trans,
                pypastix.diag.Unit, x)
        elif self.factotype == pypastix.factotype.LLT:
            pypastix.subtask_trsm(
                self.pastix_data, pypastix.side.Left,
                pypastix.uplo.Lower, pypastix.trans.Trans,
                pypastix.diag.NonUnit, x)
        else: # LU
            pypastix.subtask_trsm(
                self.pastix_data, pypastix.side.Left,
                pypastix.uplo.Upper, pypastix.trans.NoTrans,
                pypastix.diag.NonUnit, x)
        pypastix.subtask_applyorder(
            self.pastix_data, pypastix.dir.Backward, x)
        if self.check and self.x0 is not None:
            self.spmA.checkAxb(self.x0, b, x)
        return x

class Mumps(LinearOperator):
    """Factorize a matrix using mumps

    Three modes:
    - sequential: comm is None and A is a Scipy Sparse Matrix
    - distributed in global ordering: comm is not None
      and A is a Scipy Sparse Matrix
    - distributed in local ordering: A is a DistMatrix
      the communicator used is A.dd.comm, comm is not used


    Optional parameters are:
    - verbose=True/False. Default: False
    - symmetry=0 (General), 1 (Symmetric), 2 (SPD). Default: 0.
      If symmetry>0, only the lower triangular part of A is used.
    - comm, optional. Default: None
    """

    defaults = {"verbose": False,
                "symmetry": False,
                "comm"   : None,
                "ordering": "auto"}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        # We split the function in two part to be able
        # to easily add the Schur code in between
        self.init()
        self.factorize()

    def init(self):
        self.driver = pymumps.Mumps('D')
        id = self.driver.id
        A = self.A

        # Set id.par, id.comm and id.sym before init
        id.par = 1 # Process 0 takes part in the facto

        if ssp.issparse(A):
            if self.comm is None:
                # sequential
                self.driver.ICNTL[18] = 0 # centralized entry
                self.comm = MPI.COMM_SELF
            else:
                # distributed with global ordering
                self.driver.ICNTL[18] = 3 # distributed entry
            self.A_internal = A
        elif isinstance(A, DistMatrix):
            # Distributed matrix in local ordering
            self.driver.ICNTL[18] = 3 # distributed entry
            # internally, we switch to global ordering
            dd = A.dd
            self.A_internal = ssp.coo_matrix(
                dd.Ri.T.dot(
                    ssp.csc_matrix(A.local).dot(
                        dd.Ri)))
            self.comm = A.dd.comm
        id.comm_fortran = self.comm.py2f()

        if self.ordering == "auto":
           self.driver.ICNTL[7] = 7
        elif self.ordering == "Scotch":
           self.driver.ICNTL[7] = 3
        elif self.ordering == "Metis":
           self.driver.ICNTL[7] = 5

        id.sym = self.symmetry
        if self.symmetry>0:
            id.sym = 3 - id.sym # 0 Ge, 1 SPD, 2 Sym
            self.A_internal = ssp.tril(self.A_internal)

        self.driver.initialize()

        if self.verbose:
            id.icntl[0:4] = [6, 0, 6, 3]
            self.driver.ICNTL[11] = 2 # compute main stats
        else:
            id.icntl[0:4] = [0, 0, 0, 0]

        self.driver.ICNTL[24] = 1 # null pivot detection

        self.driver.set_A(self.A_internal)

        self.is_forward = False

    def factorize(self):
        with TimeIt("MumpsDriver Analysis"):
            self.driver.drive(1) # Analysis
        with TimeIt("MumpsDriver Facto"):
            self.driver.drive(2) # Facto

    @TimeIt()
    def solve(self, b):
        A = self.A
        if ssp.issparse(A) and self.comm is not None:
            b_ = self.comm.reduce(b, root=0)
        elif isinstance(A, DistMatrix):
            b_ = b.centralize(root=0)
        else:
            b_ = b
        self.driver.set_RHS(b_)
        self.driver.drive(3)
        x_ = self.driver.get_solution()
        x_.shape = b.shape
        x = x_
        if self.comm is not None:
            self.comm.Bcast(x_, root=0)
        if isinstance(A, DistMatrix):
            x = DistVector(b.dd.Ri.dot(x_), b.dd)
        return x

    @TimeIt()
    def schur(self, A, interface, **kwargs):
        """ Perform a partial factorization and compute the Schur matrix S """
        LinearOperator.setup(self, A, **kwargs)
        self.init()
        self.driver.set_schur_listvar(interface)
        self.factorize()
        self.S = self.driver.get_schur()
        self.interface = interface
        return self.S

    @TimeIt()
    def b2f(self, b):
        """ Compute the reduced RHS f from the complete RHS b """
        self.driver.set_RHS(b)
        f = self.driver.schur_forward()
        self.is_forward=True
        f.shape = (len(f), 1)
        return f

    @TimeIt()
    def y2x(self, y, b):
        """ Compute the complete solution x from the Schur solution y """
        if not self.is_forward:
            self.b2f(b)

        x = self.driver.schur_backward(y)
        x.shape = b.shape
        self.is_forward = False
        return x

    def __del__(self):
        try:
            self.driver.finalize()
        except (TypeError, AttributeError):
            pass

def cg(A, b, x0=None, tol=1e-5, maxiter=None, xtype=None,
       M=None, callback=None, ritz=False, save_x=False,
       debug=False, true_res=False):
    """Solves the linear problem Ax=b using the
    Conjugate Gradient algorithm. Interface compatible
     with scipy.sparse.linalg.cg

    Parameters
    ----------
    A : matrix-like object
        A is the linear operator on which we want to perform
        the solve operation. The only requirement on A is to
        provide a method dot(self, x) where x is a
        vector-like object.
    b : vector-like object
        b is the right-hand side of the system to solve. The
        only requirement on b is to provide the following
        methods: __len__(self) (not necessary if maxiter is
        provided), copy(self), __add__(self, w),
        __sub__(self, w), __rmul__(self, a), dot(self, w)
        where w is a vector-like and a is a scalar
    x0 : vector-like object
        starting guess for the solution, optional
    tol : float
        Relative tolerance to achieve, optional, default: 1e-5
    maxiter : integer
        Maximum number of iterations, optional, default: len(b)
    xtype :
        not used (compatibility with scipy.sparse.linalg.cg)
    M : matrix-like object
        Preconditioner for A, optional
    callback : function
        After each iteration, callback(x) is called, where x is
        the current solution, optional
    ritz : boolean
        Store the dot products in order to compute the Ritz
        values later, optional
    save_x : boolean
        Store the value of x at each iteration, optional
    debug : boolean
        print debug info, optional
    true_res : boolean
        recompute the residual at each iteration, optional

    """
    bb = b.T.dot(b)                  # $||b||^2 = b^T b$
    maxiter = A.shape[0] if maxiter is None else maxiter

    with TimeIt("Instrumentation"):
        global _cg_n_iter
        _cg_n_iter=0
        if ritz:
            global _cg_omega, _cg_gamma
            _cg_omega = np.zeros(maxiter)
            _cg_gamma = np.zeros(maxiter)

    # Initialization
    x = 0 * b if x0 is None else x0
    r = b - A.dot(x)                 # $r = b - \A x$
    rr = r.T.dot(r)                  # $||r||^2 = r^T r$
    if rr / bb <= tol * tol:         # $\frac{||r||}{||b||} \leq \varepsilon$
        return x, 0
    z = r if M is None else M.dot(r) # $z = \M r$
    p = z.copy()                     # $p = z$
    rz = r.T.dot(z)                  # $r^T z$

    with TimeIt("Instrumentation"):
        if save_x:
            global _cg_x
            _cg_x = [x]

    for i in range(maxiter):
        Ap = A.dot(p)               # $\A p$
        alpha = (rz / (p.T.dot(Ap)))[0,0] # $\alpha = \frac{r^T z}{p^T \A p}$
        x += alpha * p              # $x = x + \alpha p$
        if true_res:
            r = b - A.dot(x)        # $r = b - \A x$
        else:
            r -= alpha * Ap         # $r = r - \alpha\ \A p$

        with TimeIt("Instrumentation"):
            if ritz:
                if i>0:
                    _cg_gamma[i-1] = beta
                _cg_omega[i] = alpha
            _cg_n_iter += 1
            if callback:
                callback(x)
            if save_x:
                _cg_x.append(x.copy())

        rr = r.T.dot(r)              # $||r||^2 = r^T r$

        if debug:
            print("Iteration: {}, "
                  "||r||_2/||b||_2 = {}"
                  .format(i, np.sqrt(rr/bb)[0, 0]))

        if rr / bb <= tol * tol:    # $\frac{||r||}{||b||} \leq \varepsilon$
            return x, 0

        z = r if M is None else M.dot(r) # $z = \M r$
        rz, rzold = r.T.dot(z), rz
        beta = (rz / rzold)[0,0]     # $\beta = \frac{r_i^T z_i}{r_{i-1}^T z_{i-1}}$
        p = z + beta * p             # $p = z + \beta p$
    return x, i

class ConjGrad(LinearOperator):
    """Solve Ax=b using the CG algorithm

    Optional parameters are:
    - x0, initial guess for the solution, default=0*b
    - tol, relative tolerance to achieve, default=1e-5
    - maxiter, maximum number of iterations, default=len(b)
    - M, preconditioner for A
    - callback, after each iteration, callback(x) is called,
      where x is the current solution
    - ritz=True/False, whether to compute the ritz values
      (approximate eigenvalues of A), default=False
    - save_x=True/False, whether to store x at each
      iteration, default=False
    - setup_M=True/False, whether to try and call
      M.setup(A) during the setup phase
    - debug=True/False, whether to print debug info
    - true_res=True/False, whether to recompute the true
      residual instead of a recurrence formula
    """

    defaults = {"x0": None,
                "tol": 1e-5,
                "maxiter": None,
                "M": None,
                "callback": None,
                "ritz": False,
                "save_x": False,
                "setup_M": True,
                "debug": False,
                "true_res": False}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        # We setup the preconditioner if possible and
        # asked for by the user
        if self.setup_M:
            if hasattr(self.M, "setup"):
                self.M.setup(A)

    @TimeIt()
    def solve(self, b):
        # For deflation, the preconditioner first
        # orthogonalize x0, through its self.M.x0 method
        x0_f = getattr(self.M, "x0", None)
        if x0_f is not None:
            r0 = b if self.x0 is None else (
                b - self.A.dot(self.x0))
            self.x0 = x0_f(r0)
        # reshape b
        if b.ndim==1:
            b = b.reshape((-1, 1))
        # Call the standalone cg function
        x, self.i = cg(self.A, b, self.x0, self.tol, self.maxiter,
                       None, self.M, self.callback, self.ritz,
                       self.save_x, self.debug)

        self.n_iter = _cg_n_iter
        self.parameters["n_iter"] = self.n_iter
        if self.ritz:
            self.omega = _cg_omega
            self.gamma = _cg_gamma
        if self.save_x:
            self.x_ = _cg_x
        return x

    def ritz_values(self):
        """Compute the ritz values of the Hessenberg matrix.

        Call this function after a solve has been performed
        with self.ritz==True
        """
        if self.n_iter>1:
            alpha = np.zeros(self.n_iter)
            alpha[0] = 1/self.omega[0]
            beta = np.zeros(self.n_iter-1)
            for i in range(self.n_iter-1):
                alpha[i+1] = (1/self.omega[i+1]
                              + self.gamma[i]/self.omega[i])
                beta[i] = (np.sqrt(max(self.gamma[i], 0))
                           / self.omega[i])
                T = (np.diag(alpha)
                     + np.diag(beta, 1)
                     + np.diag(beta, -1))
            lambda_ = la.eigvalsh(T)
        else:
            lambda_ = np.array([1])
        return lambda_

def gcr(A, b, x0=None, tol=1e-5, maxiter=None, M=None):

    bb = b.T.dot(b)                  # $||b||^2 = b^T b$
    maxiter = A.shape[0] if maxiter is None else maxiter

    with TimeIt("Instrumentation"):
        global _gcr_n_iter
        _gcr_n_iter=0

    # Initialization
    x = 0 * b if x0 is None else x0
    r = b - A.dot(x)

    rr = r.T.dot(r)
    if rr / bb <= tol * tol:
        return x, 0

    MP = []
    AMP = []
    for i in range(maxiter):
        Mr = M.dot(r) if M is not None else r.copy()
        MP.append(Mr)
        AMP.append(A.dot(MP[i]))
        # orthonormalization
        for j in range(i):           # $(\A\M p_i)^T\, \A\M p_j = 0$
            alpha = np.asscalar(AMP[i].T.dot(AMP[j]))
            MP[i] -= alpha * MP[j]
            AMP[i] -= alpha * AMP[j]
        normAMPi = np.asscalar(np.sqrt(AMP[i].T.dot(AMP[i])))
        MP[i] /= normAMPi
        AMP[i] /= normAMPi           # $||\A\M p_i||_2 = 1$
        # update x and r
        rAMPi = np.asscalar(r.T.dot(AMP[i]))
        x += rAMPi * MP[i]           # $x_{i+1} = x_i + (r_i^T \A\M p_i) \M p_i$
        r -= rAMPi * AMP[i]          # $r_{i+1} = r_i - (r_i^T \A\M p_i) \A\M p_i$

        with TimeIt("Instrumentation"):
            _gcr_n_iter += 1

        rr = r.T.dot(r)
        if rr / bb <= tol * tol:
            return x, 0

    return x, i

class GCR(LinearOperator):

    defaults = {"x0": None,
                "tol": 1e-5,
                "maxiter": None,
                "M": None,
                "setup_M": True}
    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        # We setup the preconditioner if possible and
        # asked for by the user
        if self.setup_M:
            if hasattr(self.M, "setup"):
                self.M.setup(A)

    @TimeIt()
    def solve(self, b):
        # For deflation, the preconditioner first
        # orthogonalize x0, through its self.M.x0 method
        x0_f = getattr(self.M, "x0", None)
        if x0_f is not None:
            r0 = b if self.x0 is None else (
                b - self.A.dot(self.x0))
            self.x0 = x0_f(r0)
        # reshape b
        if b.ndim==1:
            b = b.reshape((-1, 1))
        # Call the standalone gcr function
        x, self.i = gcr(self.A, b, self.x0, self.tol,
                        self.maxiter, self.M)

        self.n_iter = _gcr_n_iter
        self.parameters["n_iter"] = self.n_iter
        return x

class SchurSolver(LinearOperator):
    """ Solve a system using a Schur complement matrix

    Use local_solver to eliminate all variables in A that are
    not in interface and compute the Schur matrix S. Then, use
    interface_solver to solve S.
    """

    defaults = {"interface": None,
                "local_solver": ScipyDirectSolver,
                "interface_solver": GCR,
                "symmetry": False}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        try:
            self.S = self.local_solver.schur(self.A,
                                             self.interface,
                                             symmetry=self.symmetry)
        except AttributeError:
            interface = self.interface
            n = self.A.shape[0]
            nG = len(interface)
            nI = n - nG
            interior = np.setdiff1d(range(n), interface)
            RI = ssp.csc_matrix((np.ones_like(interior),
                                 (range(nI), interior)),
                                shape=(nI, n))    # $\RI$
            RG = ssp.csc_matrix((np.ones_like(interface),
                                 (range(nG), interface)),
                                shape=(nG, n))    # $\RG$
            self.RI, self.RG = RI, RG
            if isinstance(A, np.ndarray):
                A = ssp.csc_matrix(A)
            with TimeIt("Schur_AII"):
                AII = RI.dot(A.dot(RI.T))         # $\KII$
            self.local_solver.setup(AII,          # $\KII^{-1}$
                                    symmetry=self.symmetry)
            with TimeIt("Schur_AIG"):
                AIG = (RI.dot(A.dot(RG.T))).A     # $\KIG$
            if not self.symmetry:
                with TimeIt("Schur_AGI"):
                    AGI = (RG.dot(A.dot(RI.T))).A # $\KGI$
            with TimeIt("Schur_AGG"):
                self.S = (RG.dot(A.dot(RG.T))).A  # $\S = \KGG$
            with TimeIt("Schur_AII^{-1}AIG"):     # $\quad - \KGI \KII^{-1} \KIG$
                if self.symmetry:
                    self.S -= AIG.T.dot(self.local_solver.dot(AIG))
                else:
                    self.S -= AGI.dot(self.local_solver.dot(AIG))
        if self.interface_solver is not None:
            self.interface_solver.setup(self.S,
                                        symmetry=self.symmetry)

    @TimeIt()
    def b2f(self, b): # $\ftG = f_\G - \KGI \KII^{-1} f_\I$
        try:
            f = self.local_solver.b2f(b)
        except AttributeError:
            RI, RG = self.RI, self.RG
            bI = RI.dot(b)                   # $b_\I$
            bI_I = self.local_solver.dot(bI) # $\KII^{-1}f_\I$
            bI_I_ = RI.T.dot(bI_I)           # $[\KII^{-1}f_\I, 0]^T$
            f_ = self.A.dot(bI_I_)           # $[f_\I, \KGI \KII^{-1}f_\I]^T$
            f = RG.dot(b - f_)               # $f_\G - \KGI \KII^{-1}f_\I$
        return f

    @TimeIt()
    def y2x(self, y, b): # $u_\I = \KII^{-1}(f_\I - \KIG u_\G)$
        # $y = x_\G$
        if len(b.shape)==1:
            b = b.reshape((-1,1))
        try:
            x = self.local_solver.y2x(y, b)
        except AttributeError:
            RI, RG = self.RI, self.RG
            tmp = RG.T.dot(y)          # $[0, u_\G]^T$
            tmp = b - self.A.dot(tmp)  # $[f_\I - \KIG u_\G, f_\G - \KGG u_\G]^T$
            tmp = RI.dot(tmp)          # $f_\I - \KIG u_\G$
            tmp = self.local_solver.dot(tmp) # $\KII^{-1} (f_\I - \KIG u_\G)$
            tmp = RI.T.dot(tmp)        # $[\KII^{-1} (f_\I - \KIG u_\G), 0]^T$
            x = RG.T.dot(y) + tmp      # $[\KII^{-1} (f_\I - \KIG u_\G), u_\G]^T$
        return x

    @TimeIt()
    def solve(self, b): # solve $\K u=f$
        f = self.b2f(b) # $\ftG = f_\G - \KGI \KII^{-1} f_\I$
        param = self.interface_solver.parameters
        if "tol" in param:
            # $\frac{||r||}{||\ftG||} = \frac{||r||}{||f||} \frac{||f||}{||\ftG||}$
            bb = b.T.dot(b) # $||f||^2$
            ff = f.T.dot(f) # $||\ftG||^2$
            ratio = np.sqrt(bb/ff)[0, 0] # $\frac{||f||}{||\ftG||}$
            param["global_tol"] = param["tol"]
            param["tol"] *= ratio
        y = self.interface_solver.dot(f) # solve $\S u_\G = \ftG$
        x = self.y2x(y, b) # $u_\I = \KII^{-1} f_\I - \KIG u_\G$
        return x # $[u_\I, u_\G]^T$

class DistSchurSolver(SchurSolver):

    defaults = {"local_solver": ScipyDirectSolver,
                "interface_solver": GCR,
                "symmetry": False}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        self.interface = A.dd.interface
        self.local_schur = SchurSolver(A.local,
                                       interface=self.interface,
                                       local_solver=self.local_solver,
                                       interface_solver=None,
                                       symmetry=self.symmetry)
        self.S = DistMatrix(self.local_schur.S, A.dd.interface_dd())
        if self.interface_solver is not None:
            self.interface_solver.setup(self.S)

    @TimeIt()
    def b2f(self, b):
        bi = b.dd.D * b.local
        fi = self.local_schur.b2f(bi)
        f = DistVector(fi, self.S.dd, assemble=True)
        return f

    @TimeIt()
    def y2x(self, y, b):
        xi = self.local_schur.y2x(y.local, b.local)
        x = DistVector(xi, b.dd)
        return x

class NLagrangeSolver(DistSchurSolver):
    defaults = {"Ti": 1,
                "local_solver": ScipyDirectSolver,
                "interface_solver": GCR,
                "Sih_solver": ScipyDirectSolver,
                "symmetry": False}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        # Compute the Schur
        interface_solver = self.parameters["interface_solver"]
        self.parameters["interface_solver"] = None
        DistSchurSolver.setup(self, A, **kwargs)
        self.parameters["interface_solver"] = interface_solver
        # Factorize Sih
        self.Sih_solver.setup(self.S.local
                              + self.Ti*ssp.eye(A.dd.nG))
        # compute W
        dd = self.S.dd
        self.W = {}
        W_ = np.zeros((dd.ni, 1))
        for n, i in dd.neighbors.items():
            self.W[n] = 1 - W_[i]
            W_[i] = 1
        self.operator = self.NLOperator(self)
        self.interface_solver.setup(self.operator)

    def communicate(self, ui, mui):               # $u_i, \mu_i$
        dd = self.S.dd
        res = np.zeros_like(ui)
        ui_mui = np.hstack((ui, mui))             # $[u_i, \mu_i]$
        send = {n: ui_mui[dd.neighbors[n], :]
                for n in dd.neighbors}
        for j, uj_muj in neighborSendRecv(send):
            uj_muj = uj_muj.reshape((-1, 2))      # $\RGi \RGj^T [u_j, \mu_j]$
            uj = uj_muj[:,0:1]                    # $\RGi \RGj^T u_j$
            muj = uj_muj[:,1:2]                   # $\RGi \RGj^T \mu_j$
            i = dd.neighbors[j]
            res[i] += (muj                        # $\RGi \RGj^T \mu_j$
                       + self.Ti * self.W[j] * uj)# $\Ti W_{ij} \RGi \RGj^T u_j$
        return res               # $\sum_{j\in\N(i)}\RGi \RGj^T \mu_j + \Ti W_{ij} \RGi \RGj^T  u_j$

    # inner class representing the linear operator
    class NLOperator(object):

        def __init__(self, NLSolver):
            self.NLSolver = NLSolver
            ni = np.array(NLSolver.S.dd.ni)
            n = ni.copy()
            self.NLSolver.A.dd.comm.Allreduce(ni, n)
            self.shape = (np.asscalar(n), np.asscalar(n))

        @TimeIt()
        def dot(self, l):
            li = l.local
            SihI = self.NLSolver.Sih_solver
            ui = - SihI.dot(li)
            mui = li + self.NLSolver.Ti*ui
            li_ = li + self.NLSolver.communicate(ui, mui)
            return DistVector(li_, l.dd, compatible=False)

    @TimeIt()
    def solve(self, b):
        with TimeIt("rhs"):
            bi = b.dd.D * b.local
            fi = self.local_schur.b2f(bi) # $\ftGi$
            SihI = self.Sih_solver # $\Sih^{-1}$
            ui = SihI.dot(fi)      # $u_i = \Sih^{-1} \ftGi$
            mui = self.Ti*ui       # $\mu_i = \Ti u_i$
            # $\sum \RGi\RGj^T \mu_j + \Ti W_{ij} \RGi \RGj^T u_j$
            rhs = DistVector(self.communicate(ui, mui),
                             self.A.dd, compatible=False)
        li = self.interface_solver.solve(rhs).local # $\lambda_i$
        yi = SihI.dot(fi + li)     # $u_\Gi = \Sih^{-1} ( \ftGi + \lambda_i)$
        xi = self.local_schur.y2x(yi, b.local) # $[u_\Ii, u_\Gi]^T$
        x = DistVector(xi, b.dd)
        return x

class CentralizedSolver(LinearOperator):

    defaults = {"local_solver": ScipyDirectSolver,
                "root": None}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        with TimeIt("centralize_A"):
            self.A_global = A.centralize(self.root)
        if self.root is None or self.root==A.dd.rank:
            self.local_solver.setup(self.A_global)
            self.A_global_inv = self.local_solver

    @TimeIt()
    def solve(self, b):
        with TimeIt("centralize_b"):
            b_global = b.centralize(self.root)
        if self.root is None:
            x_global = self.A_global_inv.dot(b_global)
        else:
            if self.root==b.dd.rank:
                x_global = self.A_global_inv.dot(b_global)
            else:
                x_global = np.empty(b_global.shape,
                                    dtype=b.local.dtype)
            with TimeIt("distribute_x"):
                b.dd.comm.Bcast(x_global, root=self.root)
        xi = x_global[b.dd.global_indices, :]
        return DistVector(xi, b.dd)

class AbstractSchwarz(LinearOperator, DistMatrix):

    defaults = {"assemble": True,
                "Ti": None,
                "Di": None,
                "local_solver": ScipyDirectSolver}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)

        # Assembly step for additive Schwarz
        if self.assemble:
            self.Aih = A.dd.assemble(A.local, dim=2)
        else:
            self.Aih = A.local
        # Add $\Ti$ on the interface for Robin
        if self.Ti is not None:
            if np.isscalar(self.Ti):
                Ti = self.Ti * ssp.eye(A.dd.nG)
            elif self.Ti.shape[1] == 1:
                Ti = ssp.diags(self.Ti)
            else:
                Ti = self.Ti
            if A.dd.nI:
                RG = ssp.csc_matrix(
                    (np.ones_like(A.dd.interface),
                     (range(A.dd.nG), A.dd.interface)),
                    shape=(A.dd.nG, A.dd.ni))  # $\RG$
                Ti = RG.T.dot(Ti).dot(RG)
            self.Aih += Ti
        # Partition of unity for Neumann Neumann
        if self.Di is not None:
            self.D = self.partition_of_unity(A, self.Di)
            D_inv = ssp.diags(1./self.D.clip(1e-12))
            self.Aih = D_inv.dot(D_inv.dot(self.Aih.T).T)
            # dense.dot(sparse) => (sparse.T.dot(dense.T)).T
        # factorize $\Aih$
        if self.local_solver is not None:
            self.local_solver.setup(self.Aih)
            DistMatrix.__init__(self, self.local_solver, A.dd)

    @TimeIt()
    def solve(self, b):
        return DistMatrix.dot(self, b)

    @staticmethod
    def partition_of_unity(A, Di="matrix"):
        if Di == "multiplicity":
            mult = np.ones(A.dd.ni, dtype=np.int)
            for i in A.dd.neighbors.values():
                mult[i] += 1
            D = 1./mult
        elif Di == "matrix":
            diagA = A.local.diagonal()
            diagA_ = A.dd.assemble(diagA)
            D = diagA/diagA_
        elif Di == "boolean":
            D = A.dd.D
        else:
            raise TypeError((
                "Parameter Di in AbstractSchwarz"
                ".partition_of_unity should be 'multiplicity',"
                "'matrix' or 'boolean'. It is '{}'.").format(Di))
        return D

class AdditiveSchwarz(AbstractSchwarz):

    defaults = AbstractSchwarz.defaults.copy()
    defaults.update({"assemble": True,
                     "Ti": None,
                     "Di": None})

class NeumannNeumann(AbstractSchwarz):

    defaults = AbstractSchwarz.defaults.copy()
    defaults.update({"assemble": False,
                     "Ti": None,
                     "Di": "matrix"})

class RobinRobin(AbstractSchwarz):

    defaults = AbstractSchwarz.defaults.copy()
    defaults.update({"assemble": False,
                     "Ti": 1,
                     "Di": None})

class CoarseSolve(LinearOperator):

    defaults = {"Wi": None,
                "global_solver": CentralizedSolver}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        dd = A.dd
        if self.Wi is None:
            self.Wi = AbstractSchwarz.partition_of_unity(
                A, "matrix").reshape((-1, 1))
        Wi = self.Wi # $\Voi$
        with TimeIt("MPI assemble"):
            send = {j: (Wi.shape[1], # number of coarse vectors
                        Wi[dd.neighbors[j], :], # coarse vectors
                        list(dd.neighbors)) # neighbors
                    for j in dd.neighbors}
            recv = dict(neighborSendRecv(send))
        # We add the subdomain to the dictionary
        recv[dd.rank] = (Wi.shape[1], # number of coarse vectors
                         Wi, # coarse vectors
                         list(dd.neighbors)) # neighbors
        # Now, for each subdomain in recv, we add the corresponding
        # vectors to Wi_ and we update neighbors0
        neighbors0 = dict()
        ni0 = sum(m[0] for m in recv.values()) # number of coarse unknowns
        Wi_ = np.zeros((A.dd.ni, ni0)) # $\Voib$
        counter = 0
        for j in sorted(recv):
            n_j, Wj, neighbors_j = recv[j]
            if n_j == 0:
                continue
            if j == dd.rank:
                indices = slice(None)
            else:
                indices = dd.neighbors[j]
            Wi_[indices, counter:counter+n_j] = Wj
            for k in np.append(neighbors_j, j):
                if k != dd.rank:
                    neighbors0.setdefault(k, []).extend(
                        range(counter, counter+n_j))
            counter += n_j
        self.dd0 = DomainDecomposition(ni0, neighbors0, dd.comm)
        self.Wi_ = Wi_ # $\Voib$
        with TimeIt("coarse computeA0"):
            self.AiWi_ = A.local.dot(Wi_)       # $\Ai\Voib$
            A0i = Wi_.T.dot(self.AiWi_)         # $\Voib^T\Ai\Voib$
            self.A0 = DistMatrix(A0i, self.dd0) # $\Ao = \Vo^T\A\Vo$
        self.global_solver.setup(self.A0)
        self.A0_inv = self.global_solver        # $\Ao^{-1}$

    @TimeIt()
    def solve(self, b, project=False):
        dd = b.dd
        dd0 = self.dd0
        Wi_ = self.Wi_ # $\Voib$
        if project:    # $\Qo \ b = \Mo \A\ b$
            b0i = self.AiWi_.T.dot(b.local)
        else:          # $\Mo\ b$
            b0i = Wi_.T.dot(dd.D*b.local)
        b0 = DistVector(b0i, dd0, assemble=True)
        x0 = self.A0_inv.dot(b0)
        x = Wi_.dot(x0.local)
        return DistVector(x, dd)

    @TimeIt()
    def project(self, b): # $\Qo \ b = \Mo \A\ b$
        return self.solve(b, project=True)

    def __add__(self, other):
        return AdditivePcd(B=other, C=self)

    def __radd__(self, other):
        return self + other

class AdditivePcd(LinearOperator):

    defaults = {"B": AdditiveSchwarz,
                "C": CoarseSolve}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        if not getattr(self.B, "setup_performed", True):
            self.B.setup(A)
        if not getattr(self.C, "setup_performed", True):
            self.C.setup(A)

    @TimeIt()
    def solve(self, b):
        return self.B.dot(b) + self.C.dot(b)

class DeflatedPcd(LinearOperator):

    defaults = {"M0": CoarseSolve,
                "M1": None}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        if not getattr(self.M0, "setup_performed", True):
            self.M0.setup(A)
        if not getattr(self.M1, "setup_performed", True):
            self.M1.setup(A)

    @TimeIt()
    def solve(self, b):
        y = b if self.M1 is None else self.M1.dot(b) # $\M1\ b$
        return y - self.M0.project(y)                # $(\Id - \Qo) \M_1\ b$

    @TimeIt()
    def x0(self, b): # Compute an appropriate initial guess $x_0 = \Mo\ b$
        return self.M0.dot(b)

def eigen(A, B, B_I=None, n_v=None, dense=True, local_solver=None):
    if dense or n_v is None:
        with TimeIt("eigen_dense"):
            if ssp.issparse(A) and ssp.issparse(B):
                w, v = la.eigh(A.A, B.A)
            else:
                w, v = la.eigh(A, B)
            if n_v is None:
                n_v = A.shape[0]
            else:
                n_v = min(A.shape[0], n_v)
            w, v = w[:n_v], v[:, :n_v]
    else:
        try:
            with TimeIt("eigen_sparse"):
                if B_I is None and local_solver is not None:
                    B_I = local_solver
                    B_I.setup(B)
                w, v = sla.eigsh(A, n_v, which='SM', M=B, Minv=B_I)
        except (sla.ArpackNoConvergence, sla.ArpackError) as err:
            print(err, '=> dense computation')
            w, v = eigen(A, B, B_I, n_v, dense=True)
    return w, v

def genEO_space(M1, alpha=10, beta=10, n_v=None):
    dense = not ssp.issparse(M1.A.local)
    if isinstance(M1, NeumannNeumann):
        NN = M1
        w1, v1 = np.zeros((0)), np.zeros((M1.Aih.shape[0], 0))
    else:
        NN = NeumannNeumann(M1.A, local_solver=None)
        w1, v1 = eigen(A=NN.Aih, B=M1.Aih, n_v=n_v, dense=dense)

    if alpha > 0:
        w1 = w1[w1*alpha < 1]
        v1 = v1[:, :w1.shape[0]]

    if isinstance(M1, AdditiveSchwarz):
        AS = M1
        w2, v2 = np.zeros((0)), np.zeros((M1.Aih.shape[0], 0))
        beta = alpha
    else:
        AS = AdditiveSchwarz(M1.A, local_solver=None)
        w2, v2 = eigen(A=M1.Aih, B=AS.Aih, n_v=n_v, dense=dense)
    if beta > 0:
        w2 = w2[w2*beta < 1]
        v2 = v2[:, :w2.shape[0]]

    w, v = np.hstack((w1, w2)), np.hstack((v1, v2))
    x = np.argsort(w)

    if n_v is not None and x.shape[0] > n_v:
        x = x[:n_v]

    return v[:, x]

class GenEOPcd(LinearOperator):

    defaults = {"M1": AdditiveSchwarz,
                "local_solver": ScipyDirectSolver,
                "global_solver": CentralizedSolver,
                "deflated": True,
                "alpha": 10,
		"beta": 10,
		"n_v": 5}

    @TimeIt()
    def setup(self, A, **kwargs):
        LinearOperator.setup(self, A, **kwargs)
        if not getattr(self.M1, "setup_performed", True):
            self.M1.setup(A)

        with TimeIt("GenEO eigen") as t:
            self.Wi = genEO_space(self.M1, self.alpha, self.beta,
                                  self.n_v)
        A.dd.comm.barrier()
        self.M0 = CoarseSolve(A, Wi=self.Wi,
                              global_solver=self.global_solver)
        self.M0.parameters["Wi"] = "GenEO"
        self.parameters["t_GenEO_eigen"] = t.duration
        self.parameters["M0"] = self.M0
        if self.deflated:
            self.pcd = DeflatedPcd(A, M0=self.M0, M1=self.M1)
            self.x0 = self.pcd.x0
        else:
            self.pcd = AdditivePcd(A, B=self.M0, C=self.M1)

    @TimeIt()
    def solve(self, b):
        return self.pcd.solve(b)
