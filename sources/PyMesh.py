"""
    FsiPy Python Mesh Kernel
    ========================
"""

# External libraries
import os
import sys
import time
import copy
import vtk 
import math
import decimal
import numpy                as np
import numpy.linalg         as la
import scipy                as sp
import scipy.sparse         as ssp
import scipy.sparse.linalg  as ssla
import time                 as ti
import pickle               as pkl
from   abc                  import ABC, abstractmethod
from   mpi4py               import MPI

# Internal libraries
import ddmpy
import aux                  as aux
import pprocess             as pp
import FortranFluid         as FortranFluid
import FortranSolid         as FortranSolid
import FortranSolidImplicit as FortranSolidImplicit
from   PyParameters         import *

# Settings
np.seterr(divide='ignore', invalid='ignore')


# ============================================================================ #
#                               Mesh Class                                     #
# ============================================================================ #
class Mesh(object):

    # Initialization
    def __init__(self, d_mesh, is_parallel):

        # Attributes
        if (is_parallel == True) and (d_mesh['name']=='fluid'):
            isubdomain  = MPI.COMM_WORLD.Get_rank() + 1
            self._name  ="partition_"+str(isubdomain)       
        else:
            self._name  = d_mesh['name']       
        self._gmsh           = d_mesh['gmsh']          
        self._nnode          = d_mesh['nnode']         
        self._l_node_coords  = d_mesh['l_node_coords'] 
        self._nelem          = d_mesh['nelem']         
        self._elem_type      = d_mesh['elem_type']     
        self._nelem_surf     = d_mesh['nelem_surf']    
        self._elem_type_surf = d_mesh['elem_type_surf']



    def setCurrentToMe(self):
        """ Sets the current Gmsh model to the model with the same name as mine. """
        current_name = self._gmsh.getCurrent()
        if (current_name != self._name): self._gmsh.setCurrent(self._name)



    def getElementProperties(self, elem_type):
        self.setCurrentToMe()
        return self._gmsh.mesh.getElementProperties(elem_type)



    def getIntegrationPoints(self, elem_type, quadrature_type):
        self.setCurrentToMe()
        return self._gmsh.mesh.getIntegrationPoints(elem_type, quadrature_type)



    def getJacobian(self, elem_tag, local_coord):
        self.setCurrentToMe()
        return self._gmsh.mesh.getJacobian(elem_tag, local_coord)



    def getJacobians(self, elem_type, local_coord, tag=-1, task=0, num_tasks=1):
        self.setCurrentToMe()
        return self._gmsh.mesh.getJacobians(elem_type, local_coord, tag, task, num_tasks)



    def getBasisFunctions(self,
                          elem_type,
                          local_coord,
                          function_space_type,
                          wanted_orientations=[]):
        self.setCurrentToMe()
        return self._gmsh.mesh.getBasisFunctions(elem_type,
                                                 local_coord,
                                                 function_space_type,
                                                 wanted_orientations)



    def getLocalCoordinatesInElement(self, elem_tag, x, y, z):
        self.setCurrentToMe()
        return self._gmsh.mesh.getLocalCoordinatesInElement(elem_tag, x, y, z)



    def getElements(self, dim=-1, tag=-1):
        self.setCurrentToMe()
        return self._gmsh.mesh.getElements(dim, tag)



    def getElementByCoordinates(self, x, y, z, dim=-1, strict=True):
        self.setCurrentToMe()
        try:
            ielem,elementType,nodeTags,u,v,w = self._gmsh.mesh.getElementByCoordinates(x, y, z, dim, strict)
            return ielem
        except:
            return None



    def getElementsByType(self, elementType, tag=-1, task=0, numTasks=1):
        self.setCurrentToMe()
        return self._gmsh.mesh.getElementsByType(elementType, tag, task, numTasks)



    def getNodes(self, dim=-1, tag=-1, includeBoundary=False, returnParametricCoord=True):
        self.setCurrentToMe()
        return self._gmsh.mesh.getNodes(dim, tag, includeBoundary, returnParametricCoord)



    def getNodesForPhysicalGroup(self, dim, tag):
        self.setCurrentToMe()
        return self._gmsh.mesh.getNodesForPhysicalGroup(dim, tag)



    def getPhysicalGroups(self, dim=-1):
        self.setCurrentToMe()
        return self._gmsh.getPhysicalGroups(dim)



    def getEntitiesForPhysicalGroup(self, dim, tag):
        self.setCurrentToMe()
        return self._gmsh.getEntitiesForPhysicalGroup(dim, tag)



    def interpolateFieldToPoint(self, field, coords):
        """ Interpolates a given field to a point in space using FE interpolation. """

        self.setCurrentToMe()
        coords  = list(coords)
        ndime   = len(coords)
        coords += [0]*(3 - len(coords))
        ielem_gmsh = self.getElementByCoordinates(coords[0],
                                                  coords[1],
                                                  coords[2],
                                                  dim=ndime)

        if ielem_gmsh == None:
            print('Error! Could''t find containing element for interpolation')
            exit()

        ielem                = int(ielem_gmsh - self._l_ielems_gmsh[0])
        l_inodes             = self._l_elems[ielem,:]
        x, y, z              = coords
        elem_type            = self._elem_type
        coords_local         = self.getLocalCoordinatesInElement(ielem_gmsh, x, y, z)
        _,l_interp_weights,_ = self.getBasisFunctions(elem_type, coords_local, 'Lagrange')
        values               = field[:,l_inodes].dot(l_interp_weights)

        return values



    def get_connectivities(self):
        """ Get nodes & elements for each node """
        l_elems = self._l_elems

        l_ielems4nodes = [[] for i in range(self._nnode)]
        for ielem in range(self._nelem):
            l_inodes = l_elems[ielem,:]
            for inode in l_inodes:
                l_ielems4nodes[inode].append( ielem )

        l_inodes4nodes = [[] for i in range(self._nnode)]
        for inode in range(self._nnode):
            l_inodes = []
            for ielem in l_ielems4nodes[inode]:
                l_inodes.extend( list(l_elems[ielem,:]) )
            l_inodes = list(set(l_inodes)-{inode})
            l_inodes4nodes[inode] = l_inodes

        self._l_ielems4nodes = l_ielems4nodes
        self._l_inodes4nodes = l_inodes4nodes



    def calculate_interp_weights_fem(self, ndime, ielem, coords_target):
        """ Computes interpolation weights for Finite Element interpolation. """

        # Get gmsh element index
        ielem_gmsh_idx = self._l_ielems_gmsh[ielem]

        # Separate coordinates
        if ndime == 1:
            x = coords_target[0]
            y = 0.0
            z = 0.0
        elif ndime == 2:
            x, y = coords_target
            z = 0.0
        elif ndime == 3:
            x, y, z = coords_target

        # Get isoparametric coords and then compute FE interpolation weights
        coords_local          = self.getLocalCoordinatesInElement(ielem_gmsh_idx, x, y, z)
        _,l_interp_weights,_  = self.getBasisFunctions(self._elem_type, coords_local, 'Lagrange')

        return l_interp_weights
# ============================================================================ #
