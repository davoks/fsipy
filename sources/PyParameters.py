"""
    FsiPy Python Kernel Parameters
    ==============================
"""

# External libraries
import numpy as np

# Settings
np.seterr(divide='ignore', invalid='ignore')

# Time indices
iter_k   = 0
iter_aux = 1
time_n   = 2
time_aux = 3
