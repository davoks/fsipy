"""
    FsiPy Solid Module
    ==================
"""

# External libraries
import numpy         as np
from   mpi4py        import MPI

# Internal libraries
import PyKernel      as PyKernel
import FortranSolid  as FortranSolid
from   PyParameters  import *


class Solid(PyKernel.Problem):
    """ Solid class in Python. """

    # Initialize object 
    def __init__(self, d_params, d_mesh, d_bconds, d_parallel, rank):
        """ Initializes Problem object. """

        # Physics module name
        d_params['module_name']        = 'FortranSolid'
        # Number of past timesteps to be saved
        d_params['npast']              = 4
        # Names of variables
        self._l_variables_names        = ['acceleration',
                                          'velocity',
                                          'displacement']
        # Dimensionality of variables ['vector'|'scalar']
        self._l_variables_dims         = ['vector',
                                          'vector',
                                          'vector']
        # Type of variables ['primary'|'secondary']
        self._l_variables_types        = ['primary',
                                          'secondary',
                                          'secondary']
        # Names of equations
        self._l_equation_names         = ['explicit_solid']
                # Primary variables involved in each equation
        # It is the variable that is         
        self._l_variables_in_equations = [self._l_variables_names[0]]

        # Variables involved in boundary conditions
        # Allow us to impose values directly on a different variable thtan the primary one:
        self._d_dofs_in_bconds        = {self._l_equation_names[0]: self._l_variables_names[2]}        
        self._l_tasks_begin_timestep  = ['update_time',
                                         'update_boundary_conditions'];
        self._l_tasks_begin_equation  = [];
        self._l_tasks_begin_iteration = [];
        self._l_tasks_do_iteration    = ['impose_boundary_conditions_on_variables',        # Impose values for Dirichlet nodes on displacements
                                         'integrate_secondary_variables',                  # Calculate acceleration for Dirichlet nodes
                                         'update_boundary_conditions',                      
                                         'assemble_algebraic_system',
                                         'impose_boundary_conditions_on_algebraic_system', # Impose values for Dirichlet nodes on acceleration
                                         'solve_algebraic_system',
                                         'integrate_secondary_variables',
                                         'check_conservation',
                                         'calculate_reaction',
                                         'calculate_forces_from_stresses'];
        self._l_tasks_end_iteration   = [];
        self._l_tasks_end_equation    = [];
        self._l_tasks_end_timestep    = ['update_aux_iterative_variables',
                                         'update_aux_time_variables',
                                         'update_time_variables',
                                         'postprocess'];
        
        self._l_flags_dynamic_lhs      = []
        self._l_flags_dynamic_lumping  = []

        # Flags for dynamic assembly of LHS in each equation
        self._l_flags_dynamic_lhs      = [False]

        # Flags for dynamic lumping of LHS in correponding each equation
        self._l_flags_for_lumping  = [d_params['flag_lump_mass']]

        # Initialize Finite Element Problem object
        PyKernel.Problem.__init__(self,
                                  d_params,
                                  d_mesh,
                                  d_bconds,
                                  d_parallel,
                                  rank)
        
    def initialize_fortran_module(self):
        """ Initializes corresponding Fortran Module. """

        module_name = self._d_params['module_name']
        str_expr = module_name+'.'+module_name.lower() \
                 + '.initialize('                      \
                 + 'self._o_mesh._l_elems, '           \
                 + 'self._o_mesh._gauss_weights, '     \
                 + 'self._o_mesh._gp_jacobians, '      \
                 + 'self._o_mesh._gp_inv_jacobians, '  \
                 + 'self._o_mesh._gp_det_jacobians, '  \
                 + 'self._o_mesh._gp_coords, '         \
                 + 'self._o_mesh._gp_shape, '          \
                 + 'self._o_mesh._gp_dshape, '         \
                 + 'self._o_mesh._gp_volume, '         \
                 + 'self._rho, '                       \
                 + 'self._young, '                     \
                 + 'self._d_params[\'material\'], '    \
                 + 'self._d_params[\'dt\'], '          \
                 + 'self._d_params[\'alpha\'], '       \
                 + 'self._d_params[\'poisson\'], '     \
                 + 'self._o_mesh._nnode, '             \
                 + 'self._ndofs, '                     \
                 + 'self._npast, '                     \
                 + 'self._ndime, '                     \
                 + 'self._o_mesh._nelem, '             \
                 + 'self._o_mesh._nnode_elem, '        \
                 + 'self._o_mesh._ngaus, '             \
                 + 'self._o_mesh._nbasis) '
        exec(str_expr)



    def initialize_material_properties(self):
        """ Initializes material properties. 

            Outputs
            -------
                self._rho : numpy array of dims (nelem,ngaus)
                    solid mass density at each gauss point.
                self._young : numpy array of dims (nelem,ngaus)
                    solid Young modulus at each gauss point.
        """

        # Save mass density and kinematic viscosity at each gauss point
        self._rho   = np.zeros((self._nelem,self._o_mesh._ngaus),float)
        self._young = np.zeros((self._nelem,self._o_mesh._ngaus),float)
        for ielem in range(self._nelem):
            for igaus in range(self._o_mesh._ngaus):
                self._rho[ielem,igaus]    = self._d_params['rho']
                self._young[ielem,igaus]  = self._d_params['young']



    def compute_timestep_size(self):
        """ Calculate critical timestep.

            Outputs
            -------
                self._d_params['dt'] : float
                    time-step size.
                self._d_params['cfl'] : float
                    CFL number ( = dt / dt_critical )
                self._d_params['dt_critical'] : float
                    critical timestep size.
        """

        # For now only static timestep sizes    
        if self._tcount > 0: return

        # Compute global critical timestep and element intrinsic times
        dt_critical = 1e13
        hmin        = 1.0e13
        for ielem in range(self._nelem):
            l_inodes    = self._o_mesh._l_elems[ielem,:]
            h           = self._o_mesh._l_elem_lengths[ielem]
            wavespeed   = np.sqrt( self._d_params['young']/self._d_params['rho'] )
            dt_element  = h / wavespeed

            # Global critical timestep
            dt_critical = min(dt_critical, dt_element)
            hmin        = min([hmin, h])

        # Timestep from critical
        if self._d_params['time_coupling'] == 'from_critical':
            dt  = self._d_params['cfl'] * dt_critical
            cfl = self._d_params['cfl']
        # Fixed timestep
        else:
            dt  = self._d_params['dt']
            cfl = self._d_params['dt'] / dt_critical

        # Save to dict
        self._d_params['dt']            = dt
        self._d_params['cfl']           = cfl
        self._d_params['dt_critical']   = dt_critical
        self._d_params['min_elem_size'] = hmin


    def integrate_secondary_variables(self, iequation):
        """ Time integration of secondary variables which varies depending if
            the solid is advected externally or not.
        """
        if self._flag_advected_nodes:
            self.integrate_secondary_variables_advected()
        else:
            self.integrate_secondary_variables_usual()



    def integrate_secondary_variables_usual(self):
        """ Time integration of secondary variables such as velocity and displacement
            using Belytschko's explicit time integration scheme described in Belytschko
            box 6.1.

            Explicit-time integration flowchart:

            0. Initialization: d=0, v=0, a=0
            1. impose_boundary_conditions_on_variables:
                 d(n+1) = g(n+1)                         on dirichlet boundaries
            2. integrate_secondary_variables:
                 a) v(n+1/2) = v(n) + dt/2*a(n)         in volume
                 b) d(n+1) = d(n) + dt*v(n+1/2)         in volume
                 c) q(n+1) = [d(n+1) - d(n-1)] / dt^2   on dirichlet boundaries
            3. assemble_algebraic_system:
                 assemble A & b
            4. impose_boundary_conditions_on_algebraic_system:
                 a) A[i,:] = delta_ij;  b[i] = q[i]     on dirichlet boundaries
                 b) b += f_neumann                      on neumann boundaries
            5. solve_algebraic_system:
                 a(n+1) = A^-1 * b
            6. integrate_secondary_variables:
                 v(n+1) = v(n+1/2) + (t(n+1)-t(n+1/2))*a(n+1)  in volume
            7. check_conservation:
                 |Wkin + Wint - Wext| < epsilon * max{Wext, Wint, Wkin}
        """

        # Load time-step size
        dt = self._d_params['dt']

        # Get list of free and dirichlet degrees of freedom
        l_dir   =  self._d_dirichlet_dofs[self._l_equation_names[0]]
        l_free  =  self._d_free_dofs[self._l_equation_names[0]]     
        l_nfree = ~self._d_free_dofs[self._l_equation_names[0]]     
        l_cou   =  self._d_coupling_dofs[self._l_equation_names[0]] 
        l_ncou  = ~self._d_coupling_dofs[self._l_equation_names[0]] 

        if self.ipartial_update == 0:
            self.ipartial_update = 1
            # 1st partial update of nodal velocities:
            #
            # v(n+1/2) = v(n) + dt/2*a(n)
            #
            self._trial_velocity = self._d_variables['velocity'][:,:,iter_k] \
                         + .5*dt * self._d_variables['acceleration'][:,:,time_n]
            # Update free nodal displacements using half-time velocity:
            #
            # d(n+1) = d(n) + dt*v(n+1/2)
            #
            self._d_variables['displacement'][l_free,iter_k] = self._d_variables['displacement'][l_free,time_n] \
                                                        + dt * self._trial_velocity[l_free]
            # Update coupled nodal displacements using velocity:
            #
            # d(n+1) = d(n) + dt*v(n)
            #
            self._d_variables['displacement'][l_cou,iter_k] = self._d_variables['displacement'][l_cou,time_n] \
                                                       + dt * self._d_variables['velocity'][l_cou,iter_k]
            # Update acceleration used for Dirichlet nodes:
            #
            # a(n+1) = [d(n+1) - 2d(n) + d(n-1)] / dt^2 on boundaries
            #
            self._d_variables['acceleration'][l_dir,iter_k] = 1.0/dt**2 \
                                                              * (self._d_variables['displacement'][l_dir,iter_k] \
                                                          -2. *  self._d_variables['displacement'][l_dir,time_n]  \
                                                              +  self._d_variables['displacement'][l_dir,time_aux])
        elif self.ipartial_update == 1:
            self.ipartial_update = 0
            # Update acceleration used for coupled nodes:
            #
            # a(n+1) = [d(n+1) - 2d(n) + d(n-1)] / dt^2 on boundaries
            #
            self._d_variables['acceleration'][l_cou,iter_k] = 1.0/dt**2 \
                                                              * (self._d_variables['displacement'][l_cou,iter_k] \
                                                          -2. *  self._d_variables['displacement'][l_cou,time_n]  \
                                                              +  self._d_variables['displacement'][l_cou,time_aux])
            # 2nd partial update of nodal velocities:
            #
            # v(n+1) = v(n+1/2) + dt/2*a(n+1)
            #
            self._d_variables['velocity'][l_ncou,iter_k] = self._trial_velocity[l_ncou] \
                                                 + .5*dt * self._d_variables['acceleration'][l_ncou,iter_k]
            #
            # Update coordinates of deformed mesh
            # 
            self._o_mesh._l_node_coords_ale = self._o_mesh._l_node_coords \
                                            + self._d_variables['displacement'][:,:,iter_k].T



    def integrate_secondary_variables_advected(self):
        """ Time integration of secondary variables in the case that the solid
            is advectes externally (as is the case of the IB scheme).
        """

        # Load time-step size
        dt = self._d_params['dt']

        # Get list of free and dirichlet degrees of freedom
        l_cou = self._d_coupling_dofs[self._l_equation_names[0]] 
        l_dir = self._d_dirichlet_dofs[self._l_equation_names[0]]

        if self.ipartial_update == 0:
            self.ipartial_update = 1
            # Update nodal dirichlet velocities:
            #
            # v_dirichlet(n+1) = [d_dirichlet(n+1) - d_dirichlet(n)]/dt
            #
            self._d_variables['velocity'][l_dir,iter_k] = (self._d_variables['displacement'][l_dir,iter_k] \
                                                         - self._d_variables['displacement'][l_dir,time_n])/dt
            # Update nodal displacements of advected nodes:
            #
            # d_free(n+1) = d_free(n) + dt*v_free(n+1)
            #
            self._d_variables['displacement'][l_cou,iter_k] = self._d_variables['displacement'][l_cou,time_n] \
                                                       + dt * self._d_variables['velocity'][l_cou,iter_k]

            # Update nodal dirichlet accelerations:
            #
            # a_dirichlet(n+1) = [d_dirichlet(n+1) - 2d_dirichlet(n) - d_dirichlet(n-1)] / dt^2
            #
            self._d_variables['acceleration'][l_dir,iter_k] = (self._d_variables['displacement'][l_dir,iter_k] \
                                                           -2.*self._d_variables['displacement'][l_dir,time_n]  \
                                                              +self._d_variables['displacement'][l_dir,time_aux]) \
                                                              /dt**2

        elif self.ipartial_update == 1:
            self.ipartial_update = 0
            # Correct acceleration with value calculated from advection velocity:
            #
            # a_free(n+1) = [v_free(n+1) - v_free(n)]/dt
            #
            self._d_variables['acceleration'][l_cou,iter_k] = (self._d_variables['velocity'][l_cou,iter_k]  \
                                                             - self._d_variables['velocity'][l_cou,time_n])/dt
            #
            # Update coordinates of deformed mesh
            # 
            self._o_mesh._l_node_coords_ale = self._o_mesh._l_node_coords \
                                            + self._d_variables['displacement'][:,:,iter_k].T



    def activate_solid_advection(self):
        """ Activates flag for advection of solid nodes. """

        self._flag_advected_nodes = True



    def update_displacements(self, ddisplacements, l_dofs, flag_upd_current_nodes=True):
        """ Updates node displacements at degrees of freedom given by l_dofs
            with increments given by ddisplacements. Total node coordinates
            in the current configuration are updated by default unless 
            flag_upd_current_nodes is set to False.
        """

        if np.ndim(ddisplacements) == 1:
            ddisplacements = ddisplacements.reshape((self._ndime,-1),order='F')

        self._d_variables['displacement'][l_dofs,iter_k] += ddisplacements[l_dofs]

        if flag_upd_current_nodes:
            self._o_mesh._l_node_coords_ale = \
                  self._o_mesh._l_node_coords \
                + self._d_variables['displacement'][:,:,iter_k].T



    def check_conservation(self, iequation):
        """ Checks energy balance:

            |Wkin + Wint - Wext| < epsilon * max{Wext, Wint, Wkin}

            *TODO* : implement energy balance checking.
        """
        return None



    def print_parameters(self):

        if MPI.COMM_WORLD.Get_rank() == 0:        
            print('\nProblem Parameters:')
            print('-------------------\n')
            print('    Physics:             ',self._d_params['physics'])
            print('    Material model:      ',self._d_params['material'])
            print('    Mass density:        ',self._d_params['rho'])
            print('    Young modulus:       ',self._d_params['young'])
            print('    Poisson ratio:       ',self._d_params['poisson'])
            print('    CFL Number:          ',self._d_params['cfl'])
            print('    Timestep:            ',self._d_params['dt'])
            print('    Rayleigh alpha coef: ',self._d_params['alpha'])
            print('    Number of elements:  ',self._nelem)
            print('    Number of nodes:     ',self._nnode)
            print('    Minimum element size:',self._d_params['min_elem_size'])
            print('    Volume Quadrature:   ',self._d_params['volume_quadrature'])
            print('    Surface Quadrature:  ',self._d_params['surface_quadrature'])
            print(' ')
