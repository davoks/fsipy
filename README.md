# FsiPy

## Description:

A **Finite Element code** for simulating **immersed Fluid-structure Interaction**
problems. The program is developed in *Python3* with a *Fortran* kernel to
speed-up assembly operations.


## Usage:


**0. Load modules**

- In Marenostrum4:      `$ module purge; module load intel python/3-intel-2019.2 fabric/1.5.3`
- In Nord3:             `$ module purge; module load intel/2021.4 impi/2021.4 mkl/2021.4 bsc/current python/3.7.12`

**1.  Compile:**

- In Nord3, the file sources/Makefile is ready to use
- In Marenostrum4, change sources/Makefile such that `EM = cpython-37m-x86_64-linux-gnu.so`
- In other computers, you have to change `FC, EM, and EM` variables in file sources/Makefile

Then, execute the following lines from the FsiPy folder

1. `$ cd sources`
2. `$ make`

**2.  Run:**

- Sequential run: `$ python3 Main.py <PATH_TO_INPUTS>/<CASENAME>`
- Parallel run:   `$ mpirun -np <NUM_PROCESSORS> python3 Main.py <PATH_TO_INPUTS>/<CASENAME>`


*Note:* A folder called `partitions` will be created in a particular folder 
        only the first execution time. The same partition will be used from
        now on until the folder partitions be erased.   
        Thus, if you decide to change the number of processors for an execution
        and there exists previously a folder `partitions`, you have to delete it.  

**3.  Post-process to get .pvd time-step matched files:**

- Sequential run: `$ pprocess.py <PATH_TO_OUTPUTS>/<CASENAME>`
- Parallel run:   `$ pprocess.py <PATH_TO_OUTPUTS>/<CASENAME>` -p

**4.  Run Test-Suite. Test-Suite is working only in MN4 for the moment**

- Sequential tests:                    `$ pytest -v -p no:warnings`
- Parallel test (5 cpus is mandatory): `$ mpirun -np 5 pytest -p no:warnings`


*Note:* If the number of processors required for the run
        or test is more than those availabe, the --oversubscribe
        flag can be added to the MPI call:
        `$ mpirun --oversubscribe -np <NUM_PROCESSORS_FOR_TEST> <EXECUTABLE>`
